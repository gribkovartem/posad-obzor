<?php

namespace app\controllers;

use app\models\ChangePasswordForm;
use Yii;
use app\models\User;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\filters\AccessControl;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends CController
{
    public $layout = 'lk';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],

            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'edit'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => User::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $user_id
     * @return mixed
     */
    public function actionView($user_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($user_id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'user_id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionEdit($user_id)
    {
        return $this->render('edit', [
            'model' => $this->findModel($user_id),
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $user_id
     * @return mixed
     */
    public function actionUpdate($user_id)
    {
        $model = $this->findModel($user_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->redirect(['/user/edit', 'user_id' => \Yii::$app->user->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $user_id
     * @return mixed
     */
    public function actionDelete($user_id)
    {
        $this->findModel($user_id)->delete();

        return $this->redirect(['index']);
    }

    public function actionCp()
    {
        $model = new ChangePasswordForm();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $user = User::findOne(Yii::$app->user->id);
            $user->password = crypt($model->password, $user->salt);

            if ($user->save()) {
                $this->redirect(['/user/edit', 'user_id' => Yii::$app->user->id]);
            }
        }
    }

    public function actionUploadavatar($user_id)
    {
        $model = new User();
        $model->avatar = UploadedFile::getInstance($model, 'avatar');
        if ($model->validate(['avatar'])) {
            if (!is_dir(Yii::getAlias('@webroot') . '/upload/user/'.$user_id)) {
                mkdir(Yii::getAlias('@webroot') . '/upload/user/' . $user_id);
            }
            $model->avatar->saveAs(Yii::getAlias('@webroot') . '/upload/user/' . $user_id . '/avatar.png');
            $image = Yii::$app->image->load(Yii::getAlias('@webroot') . '/upload/user/' . $user_id . '/avatar.png');
            if ($image->resize(155, false)->save()) {
                $this->redirect(['/user/edit', 'user_id' => $user_id]);
            }
        } else {
            $this->redirect(['/user/edit', 'user_id' => $user_id]);
        }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $user_id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($user_id)
    {
        if (($model = User::findOne($user_id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
