<?php

namespace app\controllers;

use app\helpers\FilesHelper;
use app\models\CompanyCategory;
use app\models\CompanyCompanyCategory;
use Yii;
use app\models\Company;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use dosamigos\transliterator\TransliteratorHelper;
use yii\sphinx\Query;
use yii\widgets\ListView;

/**
 * CompanyController implements the CRUD actions for Company model.
 */
class CompanyController extends CController
{
    public $layout = 'lk';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Company models.
     * @param integer $user_id
     * @return mixed
     */
    public function actionIndex($user_id)
    {
        if ($user_id == Yii::$app->user->id) {
            $dataProvider = new ActiveDataProvider([
                'query' => Company::find()->where(['c_user_id' => Yii::$app->user->id])->orderBy('id DESC'),
            ]);

            $dataProvider->pagination = [
                'defaultPageSize' => 100,
                'pageSizeLimit' => [100, 100],
            ];

            return $this->render('index', [
                'dataProvider' => $dataProvider,
            ]);
        } else {
            $company = Company::find();
            return $this->render('index', [
                'model' => $company,
            ]);
        }
    }

    public function actionList($category, $subcategory = '')
    {
        $this->layout = 'inner';

        if (isset($category) && $subcategory == '') {
            $category_id = CompanyCategory::findOne(['alias' => $category])->id;
            $dataProvider = new ActiveDataProvider([
                'query' => Company::find()
                    ->join('join', 'company_company_category', 'company_company_category.company_id = company.id')
                    ->join('join', 'company_category', 'company_category.id = company_company_category.category_id')
                    ->where(['checked' => true])
                    ->andWhere(['company_category.id' => $category_id])
                    ->orderBy('id DESC'),
            ]);
        } else if (isset($subcategory) && $subcategory != '') {
            $subcategory_id = CompanyCategory::findOne(['alias' => $subcategory])->id;
            $dataProvider = new ActiveDataProvider([
                'query' => Company::find()
                    ->join('join', 'company_company_category', 'company_company_category.company_id = company.id')
                    ->join('join', 'company_category', 'company_category.id = company_company_category.subcategory_id')
                    ->where(['checked' => true])
                    ->andWhere(['company_category.id' => $subcategory_id])
                    ->orderBy('id DESC'),
            ]);
        } else {
            $dataProvider = new ActiveDataProvider([
                'query' => Company::find()->where(['checked' => true])->orderBy('id DESC'),
            ]);
        }

        $dataProvider->pagination = [
            'defaultPageSize' => 4,
            'pageSizeLimit' => [4, 100],
        ];

        return $this->render('list', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSearch()
    {
        $this->layout = 'inner';

        $q = (!empty(Yii::$app->request->get()['q'])) ? Yii::$app->request->get()['q'] : ' ';

        $query = new Query;
        $query = $query->from('company')->match($q);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->pagination = [
            'defaultPageSize' => 4,
            'pageSizeLimit' => [4, 100],
        ];

        return $this->render('search', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Company model.
     * @param string $alias
     * @return mixed
     */
    public function actionView($alias)
    {
        $this->layout = 'inner';
        $model = Company::findOne(['alias' => $alias]);

        if ($model->checked) {
            return $this->render('view', [
                'model' => $model,
            ]);
        } else {
            echo 123; exit;
        }
    }

    /**
     * Creates a new Company model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer $user_id
     * @return mixed
     */
    public function actionCreate($user_id)
    {
        $company = new Company();

        if (isset(Yii::$app->request->post()['Company'])) {
            $company->load(Yii::$app->request->post());
            $company->alias = mb_strtolower(str_replace(' ', '-',
                TransliteratorHelper::process($company->name, '', 'en')), 'utf8');
            $company->c_user_id = Yii::$app->user->id;
            $company->site_url = str_replace('http://', '', Yii::$app->request->post()['Company']['site_url']);

            if ($company->save()) {
                exec('sh /home/iqdev/reindex_sphinx.sh');
                return $this->redirect(['/company/index', 'user_id' => $user_id, '#' => 'content']);
            }
        } else {
            //удалить папку, созданную при добавлении компании, если компанию чувак так и не добавил и ушел со страницы
            $new_company_id = Company::find()->orderBy('id DESC')->one()->id + 1;
            if (is_dir(Yii::getAlias('@webroot') . '/upload/company/'.$new_company_id)) {
                FilesHelper::removeDirectory(Yii::getAlias('@webroot') . '/upload/company/'.$new_company_id);
            }
        }

        return $this->render('lk/create', [
            'model' => $company,
        ]);
    }

    public function actionEdit($id, $user_id)
    {
        $company = $this->findModel($id);

        if (isset(Yii::$app->request->post()['Company'])) {
            $company->vk = '';
            $company->fb = '';
            $company->inst = '';
            $company->ok = '';
            $company->mail = '';
            $company->load(Yii::$app->request->post());
            $company->c_user_id = Yii::$app->user->id;
            $company->site_url = str_replace('http://', '', Yii::$app->request->post()['Company']['site_url']);
            $company->site_url = str_replace('https://', '', Yii::$app->request->post()['Company']['site_url']);
            $company->site_url = str_replace('/', '', Yii::$app->request->post()['Company']['site_url']);

            if ($company->save()) {
                return $this->redirect(['/company/index', 'user_id' => $user_id, '#' => 'content']);
            } else {
                return $this->render('lk/edit', [
                    'model' => $company,
                ]);
            }
        }

        return $this->render('lk/edit', [
            'model' => $company,
        ]);
    }

    /**
     * Updates an existing Company model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param integer $user_id
     * @return mixed
     */
    public function actionUpdate($id, $user_id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Company model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        FilesHelper::removeDirectory(Yii::getAlias('@webroot') . '/upload/company/'.$id);

        return $this->redirect(['index']);
    }

    public function actionDeleterubric()
    {
        echo CompanyCompanyCategory::findOne(Yii::$app->request->post()['id'])->delete(); exit;
    }

    public function actionRemoveimage($id)
    {
        unlink(Yii::getAlias('@webroot') . '/upload/company/'.$id.'/gallery/'.Yii::$app->request->post()['image_id'].'.png');
        unlink(Yii::getAlias('@webroot') . '/upload/company/'.$id.'/gallery/66/'.Yii::$app->request->post()['image_id'].'.png');
        unlink(Yii::getAlias('@webroot') . '/upload/company/'.$id.'/gallery/80/'.Yii::$app->request->post()['image_id'].'.png');
    }

    public function actionUpload($id, $type)
    {
        $model = new Company();

        if ($type == 'g') {
            $model->gallery_image = UploadedFile::getInstance($model, 'gallery_image');
            if ($model->validate(['gallery_image'])) {
                if (!is_dir(Yii::getAlias('@webroot') . '/upload/company/'.$id)) {
                    mkdir(Yii::getAlias('@webroot') . '/upload/company/'.$id);
                }
                if (!is_dir(Yii::getAlias('@webroot') . '/upload/company/'.$id.'/gallery')) {
                    mkdir(Yii::getAlias('@webroot') . '/upload/company/'.$id.'/gallery');
                }
                if (!is_dir(Yii::getAlias('@webroot') . '/upload/company/'.$id.'/gallery/66')) {
                    mkdir(Yii::getAlias('@webroot') . '/upload/company/'.$id.'/gallery/66');
                }
                if (!is_dir(Yii::getAlias('@webroot') . '/upload/company/'.$id.'/gallery/80')) {
                    mkdir(Yii::getAlias('@webroot') . '/upload/company/'.$id.'/gallery/80');
                }
                $last_image_name = scandir(Yii::getAlias('@webroot') . '/upload/company/'.$id.'/gallery/66');
                natsort($last_image_name);

                if (count($last_image_name) > 2) {
                    $last_image_name = explode('.', end($last_image_name));
                    $last_image_name = $last_image_name[0] + 1;
                } else {
                    $last_image_name = '1';
                }

                if ((count(scandir(Yii::getAlias('@webroot') . '/upload/company/'.$id.'/gallery')) - 4) < 12) {
                    $model->gallery_image->saveAs(Yii::getAlias('@webroot') . '/upload/company/' . $id . '/gallery/'.$last_image_name.'.png');
                    $image = Yii::$app->image->load(Yii::getAlias('@webroot') . '/upload/company/' . $id . '/gallery/'.$last_image_name.'.png');
                    $image->save();
                    $image = Yii::$app->image->load(Yii::getAlias('@webroot') . '/upload/company/' . $id . '/gallery/'.$last_image_name.'.png');
                    $image->resize(66, false)->save(Yii::getAlias('@webroot') . '/upload/company/' . $id . '/gallery/66/'.$last_image_name.'.png');
                    $image = Yii::$app->image->load(Yii::getAlias('@webroot') . '/upload/company/' . $id . '/gallery/'.$last_image_name.'.png');
                    $image->resize(80, false)->save(Yii::getAlias('@webroot') . '/upload/company/' . $id . '/gallery/80/'.$last_image_name.'.png');
                } else {
                    exit;
                }
            } else {
                $this->redirect(['/company/edit', 'id' => $id, 'user_id' => Yii::$app->user->id]);
            }
        } else if ($type == 'l') {
            $model->logo = UploadedFile::getInstance($model, 'logo');
            if ($model->validate(['logo'])) {
                if (!is_dir(Yii::getAlias('@webroot') . '/upload/company/'.$id)) {
                    mkdir(Yii::getAlias('@webroot') . '/upload/company/' . $id);
                }
                $model->logo->saveAs(Yii::getAlias('@webroot') . '/upload/company/' . $id . '/logo.png');
                $image = Yii::$app->image->load(Yii::getAlias('@webroot') . '/upload/company/' . $id . '/logo.png');
                if ($image->resize(120, false)->save()) {
                    $this->redirect(['/company/edit', 'id' => $id, 'user_id' => Yii::$app->user->id]);
                }
            } else {
                $this->redirect(['/company/edit', 'id' => $id, 'user_id' => Yii::$app->user->id]);
            }
        }
    }

    public function actionAjax()
    {
        if (isset($_POST['subcategories'])) {
            $query = Company::find()
                ->join('join', 'company_company_category', 'company_company_category.company_id = company.id')
                ->join('join', 'company_category', 'company_category.id = company_company_category.subcategory_id')
                ->where(['checked' => true])
                ->andWhere(['company_category.id' => $_POST['subcategories'][0]]);
            foreach ($_POST['subcategories'] as $key => $subcategory_id) {
                if ($key !== 0) $query->orWhere(['company_category.id' => $subcategory_id]);
            }
            $query->orderBy('id DESC');

            $dataProvider = new ActiveDataProvider([
                'query' => $query
            ]);
        } else {
            $dataProvider = new ActiveDataProvider([
                'query' => Company::find()
                    ->join('join', 'company_company_category', 'company_company_category.company_id = company.id')
                    ->join('join', 'company_category', 'company_category.id = company_company_category.category_id')
                    ->where(['checked' => true])
                    ->andWhere(['company_category.id' => $_POST['category_id']])
                    ->orderBy('id DESC'),
            ]);
        }

        $dataProvider->pagination = [
            'defaultPageSize' => 10,
            'pageSizeLimit' => [10, 100],
        ];

        print_r(ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '/company/_view',
            'layout' => "{items}\n{pager}",
            'pager' => [
                'prevPageLabel' => '',
                'nextPageLabel' => '',
            ],
            'emptyText' => 'Ничего не найдено',]));
        exit;
    }

    /**
     * Finds the Company model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Company the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Company::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
