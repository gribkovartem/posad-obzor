<?php

namespace app\controllers;

use app\models\User;
use Yii;
use yii\base\Security;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;
use app\models\CompanyCategory;
use dosamigos\transliterator\TransliteratorHelper;

class SiteController extends CController
{
    public $layout = 'main';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionSignup()
    {
        $login_form = new LoginForm();
        $security = new Security();
        $model = new User();
        $model->attributes = Yii::$app->request->post()['User'];
        $model->salt = $security->generateRandomString(12);
        $model->repassword = crypt($model->password, $model->salt);
        $model->password = crypt($model->password, $model->salt);
        $model->c_date = date('Y-m-d H:i:s');

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->save()) {
            if ($login_form->load(['LoginForm' => [
                    'email' => $model->email,
                    'password' => Yii::$app->request->post()['User']['password']
                ]]) && $login_form->login()) {
                Yii::$app->response->cookies->add(new \yii\web\Cookie([
                    'name' => 'welcome',
                    'value' => '1'
                ]));
                $this->redirect(['/user/edit', 'user_id' => Yii::$app->user->id, 'welcome' => 'true']);
            } else {
                $this->redirect('/');
            }
        }
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $this->redirect(['/user/edit', 'user_id' => Yii::$app->user->id]);
        } else {
            return $this->render('index', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionSpravochnik()
    {
        $this->layout = 'inner';

        return $this->render('spravochnik');
    }

    public function actionTest()
    {
        exec('sh /home/iqdev/reindex_sphinx.sh', $output);
        print_r($output);
    }
}
