<?php
use yii\helpers\Url;
use app\models\CompanyCategory;
use app\models\CompanyCompanyCategory;

$company_category = CompanyCategory::find();
$parent_categories = CompanyCategory::find()->where(['parent_id' => 0])->orderBy('name ASC')->all();
?>
<div class="content-categories">
    <?php foreach ($parent_categories as $p_category): ?>
        <div class="content-categories-category">
            <div class="content-categories-category-title">
                <a href="<?=Url::toRoute([
                    '/company/list',
                    'category' => $p_category->alias,
                ])?>">*<?=$p_category->name?></a>
            </div>
            <?php foreach (CompanyCategory::find()->where(['parent_id' => $p_category->id])->orderBy('name')->all() as $c_category): ?>
                <div class="content-categories-category-subcategory">
                    <div class="content-categories-category-subcategory-title">
                        <a href="<?=Url::toRoute([
                            '/company/list',
                            'category' => $p_category->alias,
                            'subcategory' => $c_category->alias,
                        ])?>"><?=$c_category->name?></a>
                    </div>
                    <div class="content-categories-category-subcategory-count">
                        (<?=CompanyCompanyCategory::find()->where(['subcategory_id' => $c_category->id])->count()?>)
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endforeach; ?>
</div>