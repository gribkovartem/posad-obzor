<?php
use yii\helpers\Url;
use app\models\CompanyCategory;
use app\models\CompanyCompanyCategory;

$company_category = CompanyCategory::find();
$parent_categories = CompanyCategory::find()->where(['parent_id' => 0])->orderBy('name ASC')->all();
?>
<div class="content">
<div class="content-signup"></div>
<div class="content-categories">
    <?php foreach ($parent_categories as $p_key => $p_category): ?>
        <?php if ($p_key <= 3): ?>
            <div class="content-categories-category">
                <div class="content-categories-category-title">
                    <a href="<?=Url::toRoute([
                        '/company/list',
                        'category' => $p_category->alias,
                    ])?>">*<?=$p_category->name?></a>
                </div>
                <?php foreach (CompanyCategory::findAll(['parent_id' => $p_category->id]) as $c_key => $c_category): ?>
                    <?php if ($c_key <= 9): ?>
                        <div class="content-categories-category-subcategory">
                            <div class="content-categories-category-subcategory-title">
                                <a href="<?=Url::toRoute([
                                    '/company/list',
                                    'category' => $p_category->alias,
                                    'subcategory' => $c_category->alias,
                                ])?>"><?=$c_category->name?></a>
                            </div>
                            <div class="content-categories-category-subcategory-count">
                                (<?=CompanyCompanyCategory::find()->where(['subcategory_id' => $c_category->id])->count()?>)
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    <?php endforeach; ?>
    <div class="content-categories-all">
        <div class="content-categories-all-text">
            <a href="<?=Url::toRoute(['/site/spravochnik'])?>">
                Все категории (<?=$company_category->count()?>)
            </a>
        </div>
        <hr />
    </div>
</div>
<div class="content-slider-block">
<div class="content-slider">
<div class="content-slider-arrow-wrapper left"><div class="content-slider-arrow"></div></div>
<div class="content-slider-wrapper">
<div class="content-slider-wrapper-slide active">
    <div class="content-slider-wrapper-slide-item">
        <div class="content-slider-wrapper-slide-item-title">КАФЕ "ОКИ ДОКИ"</div>
        <div class="content-slider-wrapper-slide-item-body">
            <div class="content-slider-wrapper-slide-item-body-info">
                <div class="content-slider-wrapper-slide-item-body-info-main">
                    <div class="content-slider-wrapper-slide-item-body-info-main-text">Бывает так, что это не равняйтесь. Другой - тех людей которые. Репутация</div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item time">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">с 10:00 до 20:00</div>
                    </div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item price">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">Средний чек: 860р</div>
                    </div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item location">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">Ул. Карла Маркса, д.12</div>
                    </div>
                </div>
                <div class="content-slider-wrapper-slide-item-body-info-percent"><span>до</span> 20%</div>
            </div>
            <img src="/upload/images/oki-doki.png" alt="" />
            <div class="content-slider-wrapper-slide-item-body-bottom">
                <div class="content-slider-wrapper-slide-item-body-bottom-price">
                    <div class="content-slider-wrapper-slide-item-body-bottom-price__from">от 1900 руб</div>
                    <div class="content-slider-wrapper-slide-item-body-bottom-price__to">до 5900 руб</div>
                </div>
                <a class="content-slider-wrapper-slide-item-body-bottom-link" href=""></a>
            </div>
        </div>
    </div>
    <div class="content-slider-wrapper-slide-item">
        <div class="content-slider-wrapper-slide-item-title">РЕСТОРАН "ДЖОН ДЖОЛИ"</div>
        <div class="content-slider-wrapper-slide-item-body">
            <div class="content-slider-wrapper-slide-item-body-info">
                <div class="content-slider-wrapper-slide-item-body-info-main">
                    <div class="content-slider-wrapper-slide-item-body-info-main-text">Бывает так, что это не равняйтесь. Другой - тех людей которые. Репутация</div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item time">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">с 10:00 до 20:00</div>
                    </div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item price">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">Средний чек: 860р</div>
                    </div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item location">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">Ул. Карла Маркса, д.12</div>
                    </div>
                </div>
                <div class="content-slider-wrapper-slide-item-body-info-percent"><span>до</span> 45%</div>
            </div>
            <img src="/upload/images/oki-doki.png" alt="" />
            <div class="content-slider-wrapper-slide-item-body-bottom">
                <div class="content-slider-wrapper-slide-item-body-bottom-price">
                    <div class="content-slider-wrapper-slide-item-body-bottom-price__from">от 200 руб</div>
                    <div class="content-slider-wrapper-slide-item-body-bottom-price__to">до 10900 руб</div>
                </div>
                <a class="content-slider-wrapper-slide-item-body-bottom-link" href=""></a>
            </div>
        </div>
    </div>
    <div class="content-slider-wrapper-slide-item">
        <div class="content-slider-wrapper-slide-item-title">САЛОН "СТОП МАССАЖ"</div>
        <div class="content-slider-wrapper-slide-item-body">
            <div class="content-slider-wrapper-slide-item-body-info">
                <div class="content-slider-wrapper-slide-item-body-info-main">
                    <div class="content-slider-wrapper-slide-item-body-info-main-text">Бывает так, что это не равняйтесь. Другой - тех людей которые. Репутация</div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item time">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">с 10:00 до 20:00</div>
                    </div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item price">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">Средний чек: 860р</div>
                    </div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item location">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">Ул. Карла Маркса, д.12</div>
                    </div>
                </div>
                <div class="content-slider-wrapper-slide-item-body-info-percent"><span>до</span> 30%</div>
            </div>
            <img src="/upload/images/stop-massaj.png" alt="" />
            <div class="content-slider-wrapper-slide-item-body-bottom">
                <div class="content-slider-wrapper-slide-item-body-bottom-price">
                    <div class="content-slider-wrapper-slide-item-body-bottom-price__from">от 2490 руб</div>
                    <div class="content-slider-wrapper-slide-item-body-bottom-price__to">до 7900 руб</div>
                </div>
                <a class="content-slider-wrapper-slide-item-body-bottom-link" href=""></a>
            </div>
        </div>
    </div>
    <div class="content-slider-wrapper-slide-item">
        <div class="content-slider-wrapper-slide-item-title">МАГАЗИН "БАБСКИЕ ПОБРЯКУХИ"</div>
        <div class="content-slider-wrapper-slide-item-body">
            <div class="content-slider-wrapper-slide-item-body-info">
                <div class="content-slider-wrapper-slide-item-body-info-main">
                    <div class="content-slider-wrapper-slide-item-body-info-main-text">Бывает так, что это не равняйтесь. Другой - тех людей которые. Репутация</div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item time">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">с 10:00 до 20:00</div>
                    </div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item price">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">Средний чек: 860р</div>
                    </div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item location">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">Ул. Карла Маркса, д.12</div>
                    </div>
                </div>
                <div class="content-slider-wrapper-slide-item-body-info-percent"><span>до</span> 15%</div>
            </div>
            <img src="/upload/images/babskie-pobryakushki.png" alt="" />
            <div class="content-slider-wrapper-slide-item-body-bottom">
                <div class="content-slider-wrapper-slide-item-body-bottom-price">
                    <div class="content-slider-wrapper-slide-item-body-bottom-price__from">от 99 руб</div>
                    <div class="content-slider-wrapper-slide-item-body-bottom-price__to">до 2490 руб</div>
                </div>
                <a class="content-slider-wrapper-slide-item-body-bottom-link" href=""></a>
            </div>
        </div>
    </div>
</div>
<div class="content-slider-wrapper-slide">
    <div class="content-slider-wrapper-slide-item">
        <div class="content-slider-wrapper-slide-item-title">КАФЕ "ОКИ ДОКИ"</div>
        <div class="content-slider-wrapper-slide-item-body">
            <div class="content-slider-wrapper-slide-item-body-info">
                <div class="content-slider-wrapper-slide-item-body-info-main">
                    <div class="content-slider-wrapper-slide-item-body-info-main-text">Бывает так, что это не равняйтесь. Другой - тех людей которые. Репутация</div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item time">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">с 10:00 до 20:00</div>
                    </div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item price">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">Средний чек: 860р</div>
                    </div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item location">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">Ул. Карла Маркса, д.12</div>
                    </div>
                </div>
                <div class="content-slider-wrapper-slide-item-body-info-percent"><span>до</span> 20%</div>
            </div>
            <img src="/upload/images/oki-doki.png" alt="" />
            <div class="content-slider-wrapper-slide-item-body-bottom">
                <div class="content-slider-wrapper-slide-item-body-bottom-price">
                    <div class="content-slider-wrapper-slide-item-body-bottom-price__from">от 1900 руб</div>
                    <div class="content-slider-wrapper-slide-item-body-bottom-price__to">до 5900 руб</div>
                </div>
                <a class="content-slider-wrapper-slide-item-body-bottom-link" href=""></a>
            </div>
        </div>
    </div>
    <div class="content-slider-wrapper-slide-item">
        <div class="content-slider-wrapper-slide-item-title">РЕСТОРАН "ДЖОН ДЖОЛИ"</div>
        <div class="content-slider-wrapper-slide-item-body">
            <div class="content-slider-wrapper-slide-item-body-info">
                <div class="content-slider-wrapper-slide-item-body-info-main">
                    <div class="content-slider-wrapper-slide-item-body-info-main-text">Бывает так, что это не равняйтесь. Другой - тех людей которые. Репутация</div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item time">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">с 10:00 до 20:00</div>
                    </div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item price">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">Средний чек: 860р</div>
                    </div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item location">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">Ул. Карла Маркса, д.12</div>
                    </div>
                </div>
                <div class="content-slider-wrapper-slide-item-body-info-percent"><span>до</span> 45%</div>
            </div>
            <img src="/upload/images/oki-doki.png" alt="" />
            <div class="content-slider-wrapper-slide-item-body-bottom">
                <div class="content-slider-wrapper-slide-item-body-bottom-price">
                    <div class="content-slider-wrapper-slide-item-body-bottom-price__from">от 200 руб</div>
                    <div class="content-slider-wrapper-slide-item-body-bottom-price__to">до 10900 руб</div>
                </div>
                <a class="content-slider-wrapper-slide-item-body-bottom-link" href=""></a>
            </div>
        </div>
    </div>
    <div class="content-slider-wrapper-slide-item">
        <div class="content-slider-wrapper-slide-item-title">САЛОН "СТОП МАССАЖ"</div>
        <div class="content-slider-wrapper-slide-item-body">
            <div class="content-slider-wrapper-slide-item-body-info">
                <div class="content-slider-wrapper-slide-item-body-info-main">
                    <div class="content-slider-wrapper-slide-item-body-info-main-text">Бывает так, что это не равняйтесь. Другой - тех людей которые. Репутация</div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item time">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">с 10:00 до 20:00</div>
                    </div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item price">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">Средний чек: 860р</div>
                    </div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item location">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">Ул. Карла Маркса, д.12</div>
                    </div>
                </div>
                <div class="content-slider-wrapper-slide-item-body-info-percent"><span>до</span> 30%</div>
            </div>
            <img src="/upload/images/stop-massaj.png" alt="" />
            <div class="content-slider-wrapper-slide-item-body-bottom">
                <div class="content-slider-wrapper-slide-item-body-bottom-price">
                    <div class="content-slider-wrapper-slide-item-body-bottom-price__from">от 2490 руб</div>
                    <div class="content-slider-wrapper-slide-item-body-bottom-price__to">до 7900 руб</div>
                </div>
                <a class="content-slider-wrapper-slide-item-body-bottom-link" href=""></a>
            </div>
        </div>
    </div>
    <div class="content-slider-wrapper-slide-item">
        <div class="content-slider-wrapper-slide-item-title">МАГАЗИН "БАБСКИЕ ПОБРЯКУХИ"</div>
        <div class="content-slider-wrapper-slide-item-body">
            <div class="content-slider-wrapper-slide-item-body-info">
                <div class="content-slider-wrapper-slide-item-body-info-main">
                    <div class="content-slider-wrapper-slide-item-body-info-main-text">Бывает так, что это не равняйтесь. Другой - тех людей которые. Репутация</div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item time">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">с 10:00 до 20:00</div>
                    </div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item price">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">Средний чек: 860р</div>
                    </div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item location">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">Ул. Карла Маркса, д.12</div>
                    </div>
                </div>
                <div class="content-slider-wrapper-slide-item-body-info-percent"><span>до</span> 15%</div>
            </div>
            <img src="/upload/images/babskie-pobryakushki.png" alt="" />
            <div class="content-slider-wrapper-slide-item-body-bottom">
                <div class="content-slider-wrapper-slide-item-body-bottom-price">
                    <div class="content-slider-wrapper-slide-item-body-bottom-price__from">от 99 руб</div>
                    <div class="content-slider-wrapper-slide-item-body-bottom-price__to">до 2490 руб</div>
                </div>
                <a class="content-slider-wrapper-slide-item-body-bottom-link" href=""></a>
            </div>
        </div>
    </div>
</div>
<div class="content-slider-wrapper-slide">
    <div class="content-slider-wrapper-slide-item">
        <div class="content-slider-wrapper-slide-item-title">КАФЕ "ОКИ ДОКИ"</div>
        <div class="content-slider-wrapper-slide-item-body">
            <div class="content-slider-wrapper-slide-item-body-info">
                <div class="content-slider-wrapper-slide-item-body-info-main">
                    <div class="content-slider-wrapper-slide-item-body-info-main-text">Бывает так, что это не равняйтесь. Другой - тех людей которые. Репутация</div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item time">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">с 10:00 до 20:00</div>
                    </div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item price">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">Средний чек: 860р</div>
                    </div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item location">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">Ул. Карла Маркса, д.12</div>
                    </div>
                </div>
                <div class="content-slider-wrapper-slide-item-body-info-percent"><span>до</span> 20%</div>
            </div>
            <img src="/upload/images/oki-doki.png" alt="" />
            <div class="content-slider-wrapper-slide-item-body-bottom">
                <div class="content-slider-wrapper-slide-item-body-bottom-price">
                    <div class="content-slider-wrapper-slide-item-body-bottom-price__from">от 1900 руб</div>
                    <div class="content-slider-wrapper-slide-item-body-bottom-price__to">до 5900 руб</div>
                </div>
                <a class="content-slider-wrapper-slide-item-body-bottom-link" href=""></a>
            </div>
        </div>
    </div>
    <div class="content-slider-wrapper-slide-item">
        <div class="content-slider-wrapper-slide-item-title">РЕСТОРАН "ДЖОН ДЖОЛИ"</div>
        <div class="content-slider-wrapper-slide-item-body">
            <div class="content-slider-wrapper-slide-item-body-info">
                <div class="content-slider-wrapper-slide-item-body-info-main">
                    <div class="content-slider-wrapper-slide-item-body-info-main-text">Бывает так, что это не равняйтесь. Другой - тех людей которые. Репутация</div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item time">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">с 10:00 до 20:00</div>
                    </div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item price">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">Средний чек: 860р</div>
                    </div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item location">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">Ул. Карла Маркса, д.12</div>
                    </div>
                </div>
                <div class="content-slider-wrapper-slide-item-body-info-percent"><span>до</span> 45%</div>
            </div>
            <img src="/upload/images/oki-doki.png" alt="" />
            <div class="content-slider-wrapper-slide-item-body-bottom">
                <div class="content-slider-wrapper-slide-item-body-bottom-price">
                    <div class="content-slider-wrapper-slide-item-body-bottom-price__from">от 200 руб</div>
                    <div class="content-slider-wrapper-slide-item-body-bottom-price__to">до 10900 руб</div>
                </div>
                <a class="content-slider-wrapper-slide-item-body-bottom-link" href=""></a>
            </div>
        </div>
    </div>
    <div class="content-slider-wrapper-slide-item">
        <div class="content-slider-wrapper-slide-item-title">САЛОН "СТОП МАССАЖ"</div>
        <div class="content-slider-wrapper-slide-item-body">
            <div class="content-slider-wrapper-slide-item-body-info">
                <div class="content-slider-wrapper-slide-item-body-info-main">
                    <div class="content-slider-wrapper-slide-item-body-info-main-text">Бывает так, что это не равняйтесь. Другой - тех людей которые. Репутация</div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item time">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">с 10:00 до 20:00</div>
                    </div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item price">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">Средний чек: 860р</div>
                    </div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item location">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">Ул. Карла Маркса, д.12</div>
                    </div>
                </div>
                <div class="content-slider-wrapper-slide-item-body-info-percent"><span>до</span> 30%</div>
            </div>
            <img src="/upload/images/stop-massaj.png" alt="" />
            <div class="content-slider-wrapper-slide-item-body-bottom">
                <div class="content-slider-wrapper-slide-item-body-bottom-price">
                    <div class="content-slider-wrapper-slide-item-body-bottom-price__from">от 2490 руб</div>
                    <div class="content-slider-wrapper-slide-item-body-bottom-price__to">до 7900 руб</div>
                </div>
                <a class="content-slider-wrapper-slide-item-body-bottom-link" href=""></a>
            </div>
        </div>
    </div>
    <div class="content-slider-wrapper-slide-item">
        <div class="content-slider-wrapper-slide-item-title">МАГАЗИН "БАБСКИЕ ПОБРЯКУХИ"</div>
        <div class="content-slider-wrapper-slide-item-body">
            <div class="content-slider-wrapper-slide-item-body-info">
                <div class="content-slider-wrapper-slide-item-body-info-main">
                    <div class="content-slider-wrapper-slide-item-body-info-main-text">Бывает так, что это не равняйтесь. Другой - тех людей которые. Репутация</div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item time">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">с 10:00 до 20:00</div>
                    </div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item price">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">Средний чек: 860р</div>
                    </div>
                    <div class="content-slider-wrapper-slide-item-body-info-main-item location">
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__icon"></div>
                        <div class="content-slider-wrapper-slide-item-body-info-main-item__text">Ул. Карла Маркса, д.12</div>
                    </div>
                </div>
                <div class="content-slider-wrapper-slide-item-body-info-percent"><span>до</span> 15%</div>
            </div>
            <img src="/upload/images/babskie-pobryakushki.png" alt="" />
            <div class="content-slider-wrapper-slide-item-body-bottom">
                <div class="content-slider-wrapper-slide-item-body-bottom-price">
                    <div class="content-slider-wrapper-slide-item-body-bottom-price__from">от 99 руб</div>
                    <div class="content-slider-wrapper-slide-item-body-bottom-price__to">до 2490 руб</div>
                </div>
                <a class="content-slider-wrapper-slide-item-body-bottom-link" href=""></a>
            </div>
        </div>
    </div>
</div>
</div>
<div class="content-slider-arrow-wrapper right"><div class="content-slider-arrow"></div></div>
<div class="content-slider-dots">
    <div class="content-slider-dots-dot"></div>
</div>
</div>
</div>
<div class="content-news">
    <div class="content-news-title">
        <div class="content-news-title-text">НОВОСТИ ГОРОДА</div>
        <hr/>
    </div>
    <div class="content-news-wrapper">
        <article>
            <a href="">
                <img src="/upload/images/news-putin.png" alt="" />
            </a>
            <div class="content-news-wrapper-article-right">
                <div class="content-news-wrapper-article-right__title"><a href="">РЖД обещают на будущей неделе восстановить</a></div>
                <div class="content-news-wrapper-article-right__text">Накануне президент РФ Владимир Путин резко раскритиковал действия Минтранса и Дворковича,
                    курирующего транспортную отрасль, за ситуацию с отменой пригородных поездов в регионах. </div>
            </div>
        </article>
        <article>
            <a href="">
                <img src="/upload/images/news-putin.png" alt="" />
            </a>
            <div class="content-news-wrapper-article-right">
                <div class="content-news-wrapper-article-right__title"><a href="">РЖД обещают на будущей неделе восстановить</a></div>
                <div class="content-news-wrapper-article-right__text">Накануне президент РФ Владимир Путин резко раскритиковал действия Минтранса и Дворковича,
                    курирующего транспортную отрасль, за ситуацию с отменой пригородных поездов в регионах. </div>
            </div>
        </article>
        <article>
            <a href="">
                <img src="/upload/images/news-putin.png" alt="" />
            </a>
            <div class="content-news-wrapper-article-right">
                <div class="content-news-wrapper-article-right__title"><a href="">РЖД обещают на будущей неделе восстановить</a></div>
                <div class="content-news-wrapper-article-right__text">Накануне президент РФ Владимир Путин резко раскритиковал действия Минтранса и Дворковича,
                    курирующего транспортную отрасль, за ситуацию с отменой пригородных поездов в регионах. </div>
            </div>
        </article>
    </div>
</div>
</div>