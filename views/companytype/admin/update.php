<?php

use yii\helpers\Html;
use app\models\CompanyType;

/* @var $this yii\web\View */
/* @var $model app\models\CompanyType */

$this->title = 'Обновить ' . CompanyType::RUS_NAME . ': ' . $model->name;
?>
<div class="company-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
