<?php

use yii\helpers\Html;
use app\models\CompanyType;

/* @var $this yii\web\View */
/* @var $model app\models\CompanyType */

$this->title = 'Добавить ' . CompanyType::RUS_NAME;
?>
<div class="company-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
