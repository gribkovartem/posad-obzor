<?php
use yii\helpers\Html;
?>
<tr>
    <td><?= $model->id ?></td>
    <td><?= $model->name ?></td>
    <td>
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', array('/admin/default/update',
            'module' => 'companytype',
            'id' => $model->id)); ?>
    </td>
    <td>
        <?= Html::a('<span class="glyphicon glyphicon-remove"></span>', array('/admin/default/delete',
            'module' => 'companytype',
            'id' => $model->id),array(
            'class' => 'delete',
        )); ?>
    </td>
</tr>