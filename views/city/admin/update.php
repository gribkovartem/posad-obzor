<?php

use yii\helpers\Html;
use app\models\City;

/* @var $this yii\web\View */
/* @var $model app\models\City */

$this->title = 'Обновить ' . City::RUS_NAME . ': ' . $model->name;
?>
<div class="city-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
