<?php

use yii\helpers\Html;
use app\models\City;

/* @var $this yii\web\View */
/* @var $model app\models\City */

$this->title = 'Добавить ' . City::RUS_NAME;
?>
<div class="city-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
