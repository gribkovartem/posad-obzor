<?php

use \app\models\City;
use yii\widgets\ListView;

$module = (isset($_GET['module'])) ? $_GET['module'] : false;

?>

<div class="head">
    <h1><?= City::RUS_NAME ?></h1>
</div>

<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>#</th>
        <th>Название</th>
        <th>Редактировать</th>
        <th>Удалить</th>
    </tr>
    </thead>
    <tbody>
    <?= ListView::widget([
        'dataProvider'=>$dataProvider,
        'itemView'=>'//city/admin/_admin_view',
    ]) ?>
    </tbody>
</table>