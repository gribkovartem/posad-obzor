<?php

use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Мой профиль / PosadObzor';
$model = \app\models\User::findOne($model->id);
$cp_form = new \app\models\ChangePasswordForm();
?>

<div class="button-edit edit_user_info button-right">Изменить</div>
<div class="content-right-title">Профиль пользователя</div>
<div class="content-right-block">
    <div class="content-right-block-user">
        <div class="content-right-block-user-left">
            <div class="content-right-block-user-left-photo">
                <?php $form = ActiveForm::begin([
                    'options' => ['enctype'=>'multipart/form-data'],
                    'action' => '/user/'.$model->id.'/uploadavatar',
                    'fieldConfig' => [
                        'template' => "{input}",
                    ],
                ]); ?>
                <?php if (file_exists(Yii::getAlias('@webroot') . '/upload/user/'.$model->id.'/avatar.png')): ?>
                    <img src="/upload/user/<?=$model->id?>/avatar.png" alt="" />
                <?php else: ?>
                    <img src="/upload/images/__blank_avatar.png" alt="" />
                <?php endif; ?>
                <?= $form->field($model, 'avatar')->fileInput(['id' => 'user-avatar', 'class' => 'image-upload']) ?>
                <label for="user-avatar" class="content-right-block-user-left-photo__upload">загрузить аватар</label>
                <?php ActiveForm::end() ?>
            </div>
        </div>
        <div class="content-right-block-user-right">
            <?php $form = ActiveForm::begin([
                'id' => 'reg-form',
                'method' => 'post',
                'action' => \yii\helpers\Url::toRoute(['/user/update', 'user_id' => \Yii::$app->user->id, '#' => 'content']),
                'options' => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "{input}",
                ],
            ]); ?>
            <div class="content-right-block-user-right-name">
                <label for="user_name" class="content-right-block-user-right__text">
                    <?= $model->lastname . ' ' . $model->name . ' ' . $model->patronymic?>
                </label>
                <?= $form->field($model, 'lastname')->textInput([
                    'maxlength' => 255, 'class' => 'content-right-block-user-right__input', 'placeholder' => 'Фамилия'
                ]) ?>
                <?= $form->field($model, 'name')->textInput([
                    'maxlength' => 255, 'class' => 'content-right-block-user-right__input', 'placeholder' => 'Имя'
                ]) ?>
                <?= $form->field($model, 'patronymic')->textInput([
                    'maxlength' => 255, 'class' => 'content-right-block-user-right__input', 'placeholder' => 'Отчество'
                ]) ?>
            </div>
            <div class="content-right-block-user-right-phone">
                <label for="user_phone">Контактный телефон:</label>
                <div class="content-right-block-user-right__text"><?= $model->main_number ?></div>
                <?= $form->field($model, 'main_number')->textInput([
                    'maxlength' => 255, 'class' => 'content-right-block-user-right__input phone-mask'
                ]) ?>
            </div>
            <div class="content-right-block-user-right-phone">
                <label for="user_phone">Дополнительный телефон:</label>
                <div class="content-right-block-user-right__text"><?= $model->extra_number ?></div>
                <?= $form->field($model, 'extra_number')->textInput([
                    'maxlength' => 255, 'class' => 'content-right-block-user-right__input phone-mask'
                ]) ?>
            </div>
            <div class="content-right-block-user-right-email">
                <label for="user_email">Электронная почта:</label>
                <div class="content-right-block-user-right__text"><a href="mailto:<?= $model->email ?>"><?= $model->email ?></a></div>
                <?= $form->field($model, 'email')->textInput([
                    'maxlength' => 255, 'class' => 'content-right-block-user-right__input email-mask'
                ]) ?>
            </div>
            <input type="submit" class="button-save" value="Сохранить" />
            <?php ActiveForm::end(); ?>
        </div>
        <hr />
        <div class="content-right-block-user-settings">
            <div class="content-right-block-user-settings-title">Технические данные</div>
            <?php $form = ActiveForm::begin([
                'id' => 'cp-form',
                'method' => 'post',
                'action' => \yii\helpers\Url::toRoute(['/user/cp', 'user_id' => \Yii::$app->user->id, '#' => 'content']),
                'options' => ['class' => 'form-horizontal'],
                'enableAjaxValidation' => true,
                'fieldConfig' => [
                    'template' => "
                                <div class=\"content-right-block-user-settings-block\">{label}\n
                                    {input}<div class=\"field__error\">{error}</div></div>",
                ],
            ]); ?>
                <?= $form->field($cp_form, 'old_password')->passwordInput() ?>
                <?= $form->field($cp_form, 'password')->passwordInput() ?>
                <?= $form->field($cp_form, 're_password')->passwordInput() ?>
                <input type="submit" class="button-save" value="Сохранить" />
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
