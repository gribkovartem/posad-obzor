<?php

use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

$model = \app\models\User::findOne(\Yii::$app->user->id);
?>
<div class="content-title">личный кабинет</div>
<div class="content-left">
    <div class="content-left-menu">
        <a href="<?= \yii\helpers\Url::toRoute('/user/index') ?>" class="active">Общая информация</a>
        <a href="<?= \yii\helpers\Url::toRoute('/company/index') ?>">Мои компании</a>
        <a href="<?= \yii\helpers\Url::toRoute('/goods/index') ?>">Мои товары/услуги</a>
        <a href="<?= \yii\helpers\Url::toRoute('/ad/index') ?>">Мои объявления</a>
    </div>
</div>
<div class="content-right">
    <div class="content-right-block active">
        <div class="button-edit edit_user_info">Изменить</div>
        <div class="content-right-block-title">Профиль пользователя</div>
        <div class="content-right-block-user">
            <div class="content-right-block-user-left">
                <div class="content-right-block-user-left-photo">
                    <img src="/upload/images/__blank_avatar.png" alt="" />
                    <div class="content-right-block-user-left-photo__upload">загрузить аватар</div>
                </div>
            </div>
            <div class="content-right-block-user-right">
                <?php $form = ActiveForm::begin([
                    'id' => 'reg-form',
                    'method' => 'post',
                    'action' => \yii\helpers\Url::toRoute('/user/update'),
                    'options' => ['class' => 'form-horizontal'],
                    'fieldConfig' => [
                        'template' => "{input}",
                    ],
                ]); ?>
                <div class="content-right-block-user-right-name">
                    <label for="user_name" class="content-right-block-user-right__text"><?= $model->name ?></label>
                    <?= $form->field($model, 'name')->textInput(['maxlength' => 255, 'class' => 'content-right-block-user-right__input']) ?>
                </div>
                <div class="content-right-block-user-right-phone">
                    <label for="user_phone">Контактный телефон:</label>
                    <div class="content-right-block-user-right__text"><?= $model->main_number ?></div>
                    <?= $form->field($model, 'main_number')->textInput(['maxlength' => 255, 'class' => 'content-right-block-user-right__input']) ?>
                </div>
                <div class="content-right-block-user-right-phone">
                    <label for="user_phone">Дополнительный телефон:</label>
                    <div class="content-right-block-user-right__text"><?= $model->main_number ?></div>
                    <?= $form->field($model, 'extra_number')->textInput(['maxlength' => 255, 'class' => 'content-right-block-user-right__input']) ?>
                </div>
                <div class="content-right-block-user-right-email">
                    <label for="user_email">Электронная почта:</label>
                    <div class="content-right-block-user-right__text"><a href="mailto:<?= $model->email ?>"><?= $model->email ?></a></div>
                    <?= $form->field($model, 'email')->textInput(['maxlength' => 255, 'class' => 'content-right-block-user-right__input']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
            <hr />
            <div class="content-right-block-user-settings">
                <div class="content-right-block-user-settings-title">Технические данные</div>
                <form action="">
                    <div class="content-right-block-user-settings-block">
                        <label for="old_password">Старый пароль:</label>
                        <input type="password" id="old_password" />
                    </div>
                    <div class="content-right-block-user-settings-block">
                        <label for="new_password">Новый пароль:</label>
                        <input type="password" id="new_password" />
                    </div>
                    <div class="content-right-block-user-settings-block">
                        <label for="re_password">Повторите пароль:</label>
                        <input type="password" id="re_password" />
                    </div>
                    <input type="submit" class="button-edit" value="Изменить" />
                </form>
            </div>
        </div>
    </div>
    <div class="content-right-block">company</div>
    <div class="content-right-block">goods</div>
    <div class="content-right-block">ads</div>
</div>