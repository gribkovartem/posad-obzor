<?php
use yii\helpers\Html;
use app\assets\AppAsset;
use yii\bootstrap\ActiveForm;
use yii\widgets\Spaceless;
use app\models\CompanyCategory;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

AppAsset::register($this);
$model = new \app\models\User();
$login_form = new \app\models\LoginForm();
$categories = CompanyCategory::find()->where(['parent_id' => 0])->orderBy('name ASC')->all();
$q = (!empty(Yii::$app->request->get()['q'])) ? Yii::$app->request->get()['q'] : '';
?>
<?php Spaceless::begin(); ?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0">

    <?= Html::csrfMetaTags() ?>
    <title>Главная / <?= Html::encode(Yii::$app->name) ?></title>
    <?php $this->head() ?>
</head>
<body class="<?=(Yii::$app->request->cookies->getValue('welcome')) ? 'modaled' : ''?>">
<?php $this->beginBody() ?>
<div id="sign" class="modal <?=(Yii::$app->request->cookies->getValue('welcome')) ? 'active' : ''?>">
    <div class="modal-dialog">
        <div class="modal-close"></div>
        <?php if (Yii::$app->request->cookies->getValue('welcome')): ?>
            <h3>Уважаемый <?=\app\models\User::findOne(Yii::$app->user->id)->name?>,</h3><br />
            <p>Спасибо за регистрацию на портале PosadObzor!</p><br />
            <p>Регистрируясь на нашем портале</p>
            <p>Вы получаете возможность создавать объявления,</p>
            <p>добавлять свои компании в справочник, комментировать компании,</p>
            <p>а так же другие расширенные возможности.<p><br />
            <p>С уважением, Администрация портала PosadObzor</p>
        <?php else: ?>
            <div class="signup">
                <div class="signup-title">Регистрация / Авторизация</div>
                <div class="signup-block">
                    <div class="signup-block-tabs">
                        <a class="signup-block-tabs-tab active" href="#/signup/reg">Регистрация</a>
                        <a class="signup-block-tabs-tab" href="#/signup/login">Войти</a>
                    </div>
                    <div class="signup-block-content">
                        <div class="signup-block-content-item active">
                            <?php $form = ActiveForm::begin([
                                'id' => 'reg-form',
                                'method' => 'post',
                                'action' => \yii\helpers\Url::toRoute('/site/signup'),
                                'options' => ['class' => 'form-horizontal'],
                                'enableAjaxValidation' => true,
                                'fieldConfig' => [
                                    'template' => "
                                    <div class=\"signup-block-content-item-field\">{label}<span class=\"star\">*</span>\n
                                        {input}<div class=\"signup-block-content-item-field__error\">{error}</div></div>",
                                ],
                            ]); ?>

                            <?= $form->field($model, 'name') ?>
                            <?= $form->field($model, 'email')->textInput(['class' => 'email-mask']) ?>
                            <?= $form->field($model, 'password')->passwordInput() ?>
                            <?= $form->field($model, 'repassword')->passwordInput() ?>
                            <div class="signup-block-content-item-submit">
                                <input id="submit_reg" type="submit" value="Зарегистрироваться" />
                                <label for="submit_reg"></label>
                            </div>
                            <div class="signup-block-content-item__info">
                                Поля отмеченные <span class="star">*</span> обязательны для заполнения
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                        <div class="signup-block-content-item">

                            <?php $form = ActiveForm::begin([
                                'id' => 'login-form',
                                'method' => 'post',
                                'action' => \yii\helpers\Url::toRoute('/site/login'),
                                'options' => ['class' => 'form-horizontal'],
                                'enableAjaxValidation' => true,
                                'fieldConfig' => [
                                    'template' => "
                                    <div class=\"signup-block-content-item-field\">{label}<span class=\"star\">*</span>\n
                                        {input}<div class=\"signup-block-content-item-field__error\">{error}</div></div>",
                                ],
                            ]); ?>

                            <?= $form->field($login_form, 'email')->textInput(['class' => 'email-mask']) ?>
                            <?= $form->field($login_form, 'password')->passwordInput() ?>
                            <!--                            <div class="signup-block-content-item-checkbox">-->
                            <!--                                <input type="checkbox" name="LoginForm[rememberMe]" class="checkbox" id="remember_me" />-->
                            <!--                                <label for="remember_me">Запомнить меня</label>-->
                            <!--                            </div>-->
                            <div class="signup-block-content-item-submit">
                                <input id="submit_login" type="submit" value="Войти" />
                                <label for="submit_login"></label>
                            </div>
                            <div class="signup-block-content-item-oauth">
                                <div class="signup-block-content-item-oauth-text">ВОЙТИ ЧЕРЕЗ</div>
                                <div class="signup-block-content-item-oauth-icons">
                                    <a href="" class="signup-block-content-item-oauth-icons-icon vk"></a>
                                    <a href="" class="signup-block-content-item-oauth-icons-icon fb"></a>
                                    <a href="" class="signup-block-content-item-oauth-icons-icon tw"></a>
                                    <a href="" class="signup-block-content-item-oauth-icons-icon mail"></a>
                                </div>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
<header class="short">
    <div class="header-parallax__shadow"></div>
    <div class="header-content">
        <div class="header-content-signup">
            <?php if (isset(\Yii::$app->user->identity)): ?>
                <a class="header-content-signup-login" href="<?= \yii\helpers\Url::toRoute(['/user/edit', 'user_id' => \Yii::$app->user->id]) ?>">
                    <div class="header-content-signup-login text"><?= \Yii::$app->user->identity->name ?></div>
                    <div class="header-content-signup-login icon"></div>
                </a>
                <a class="header-content-signup-reg" data-method="post" href="<?= \yii\helpers\Url::toRoute('/site/logout') ?>">
                    <div class="header-content-signup-reg text">Выход</div>
                </a>
            <?php else: ?>
                <a class="header-content-signup-login open-modal" data-modal-id="sign" href="#/signup/login">
                    <div class="header-content-signup-login text">ВХОД</div>
                    <div class="header-content-signup-login icon"></div>
                </a>
                <a class="header-content-signup-reg open-modal" data-modal-id="sign" href="#/signup/reg">
                    <div class="header-content-signup-reg text">РЕГИСТРАЦИЯ</div>
                    <div class="header-content-signup-reg icon"></div>
                </a>
            <?php endif; ?>
        </div>
        <div class="header-content-title">
            <a href="/">
                <div class="header-content-title__white">СПРАВОЧНО-ИНФОРМАЦИОННЫЙ РЕСУРС </div>
                <div class="header-content-title__black">POSAD OBZOR</div>
            </a>
        </div>
        <div class="header-content-search">
            <form action="<?=Url::toRoute(['/company/search'])?>" method="get">
                <div class="header-content-search-input">
                    <input class="header-content-search-input-text" type="text" name="q"
                           value="<?=$q?>" placeholder="Я хочу найти интересный ресторан..." />
                    <div class="header-content-search-input-submit">
                        <input class="header-content-search-input-submit__button" type="submit"
                               id="header-content-search-input-submit__button" value="ИСКАТЬ" />
                        <label for="header-content-search-input-submit__button"></label>
                    </div>
                </div>
                <div class="header-content-search-checkbox">
                    <input type="checkbox" name="checkbox_service" class="checkbox" id="checkbox_service" />
                    <label for="checkbox_service">Поиск по услугам</label>

                    <input type="checkbox" name="checkbox_goods" class="checkbox" id="checkbox_goods" />
                    <label for="checkbox_goods">Поиск по товарам</label>
                </div>
            </form>
        </div>
        <div class="header-content-toggles">
            <a class="header-content-toggles-toggle menu" href="<?=Url::toRoute(['/site/spravochnik'])?>">
                <div class="header-content-toggles-toggle-text">СПРАВОЧНИК</div>
            </a>
            <a class="header-content-toggles-toggle" href="">
                <div class="header-content-toggles-toggle-text">ГДЕ ПЕРЕКУСИТЬ</div>
            </a>
            <a class="header-content-toggles-toggle" href="">
                <div class="header-content-toggles-toggle-text">ОБРАЗОВАНИЕ</div>
            </a>
            <a class="header-content-toggles-toggle" href="">
                <div class="header-content-toggles-toggle-text">ГДЕ ПЕРЕНОЧЕВАТЬ</div>
            </a>
            <a class="header-content-toggles-toggle" href="">
                <div class="header-content-toggles-toggle-text">МЕДИЦИНА</div>
            </a>
            <a class="header-content-toggles-toggle" href="">
                <div class="header-content-toggles-toggle-text">АВТОМОБИЛИ</div>
            </a>
            <a class="header-content-toggles-toggle" href="">
                <div class="header-content-toggles-toggle-text">НОЧНАЯ ЖИЗНЬ</div>
            </a>
        </div>
    </div>
    <div class="header-under_menu">
        <div class="header-under_menu-triangle"></div>
        <div class="header-under_menu-back">
            <div class="header-under_menu-panel">
                <div class="header-under_menu-wrapper" id="scrollbar">
                    <div class="header-under_menu-content">
                        <?php foreach ($categories as $category): ?>
                            <div class="header-under_menu-content-item">
                                <a href="<?=Url::toRoute([
                                    '/company/list',
                                    'category' => $category->alias
                                ])?>"><?=mb_strtoupper($category->name, 'utf8')?></a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="content">
    <?= $content ?>
</div>
<footer>
    <div class="footer-content">
        <div class="footer-content-left">Справочно-информационный ресурс POSAD-OBZOR</div>
        <div class="footer-content-right">
            <a href="http://in-quality.ru" target="_blank">
                <div class="footer-content-right-text">Сайт разработан</div>
                <div class="footer-content-right-logo"></div>
            </a>
        </div>
    </div>
</footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
<?php Spaceless::end(); ?>
