<?php

use app\models\City;
use app\models\Company;
use yii\helpers\Url;
use app\models\CompanyCategory;
use app\models\CompanyCompanyCategory;

$model = (isset($model['id'])) ? Company::findOne(['id' => $model['id']]) : $model;
$working_hours = json_decode($model->working_hours);
$category = CompanyCompanyCategory::findOne(['company_id' => $model->id]);
$img_src = (file_exists(Yii::getAlias('@webroot') . '/upload/company/'.$model->id.'/logo.png')) ?
    '/upload/company/'.$model->id.'/logo.png' : '/images/company_blank.png';

if (Yii::$app->controller->action->id == 'search' || !isset(Yii::$app->request->get()['subcategory'])) {
    $company_category = CompanyCategory::findOne(['id' => $category->category_id]);
    $company_subcategory = CompanyCategory::findOne(['id' => $category->subcategory_id]);
} else {
    $company_category = CompanyCategory::findOne(['alias' => Yii::$app->request->get()['category']]);
    $company_subcategory = CompanyCategory::findOne(['alias' => Yii::$app->request->get()['subcategory']]);
}

?>
<div class="company-list" data-subcategory-id="<?=$company_subcategory->id?>">
    <div class="company-list-left">
        <div class="company-list-left-image"><img src="<?=$img_src?>" alt=""/></div>
    </div>
    <div class="company-list-right">
        <div class="company-list-right-title">
            <a href="<?=Url::toRoute([
                '/company/view',
                'category' => $company_category->alias,
                'subcategory' => $company_subcategory->alias,
                'alias' => $model->alias
            ])?>">
                <?= $model->name ?>
            </a>
        </div>
        <div class="company-list-right-category">
            <div class="company-list-right-category__text">
                <a href="<?=Url::toRoute([
                    '/company/list',
                    'category' => $company_category->alias,
                ])?>"><?=$company_category->name?></a> /
                <a href="<?=Url::toRoute([
                    '/company/list',
                    'category' => $company_category->alias,
                    'subcategory' => $company_subcategory->alias,
                ])?>">
                    <?=$company_subcategory->name?>
                </a>
            </div>
        </div>
        <div class="company-list-right-description">
            <?= $model->short_description ?>
        </div>
        <div class="company-list-right-phone">
            <div class="company-list-right-phone__title">Тел.:</div>
            <div class="company-list-right-phone__text"><?= $model->main_number ?></div>
        </div>
        <div data-city="<?= City::find($model->city_id)->one()->name ?>" class="company-list-right-address show-map"><span><?= $model->street ?>, дом: <?= $model->house ?></span></div>
        <div class="company-list-right-time"><span>с <?= $working_hours->from ?> до <?= $working_hours->to ?></span></div>
    </div>
</div>