<?php

use yii\helpers\Url;
use app\models\CompanyType;
use app\models\CompanyCategory;
use app\models\CompanyCompanyCategory;

$category = CompanyCompanyCategory::findOne(['company_id' => $model->id]);
$company_category = CompanyCategory::findOne(['id' => $category->category_id]);
$company_subcategory = CompanyCategory::findOne(['id' => $category->subcategory_id]);
$img_src = (file_exists(Yii::getAlias('@webroot') . '/upload/company/'.$model->id.'/logo.png')) ?
    '/upload/company/'.$model->id.'/logo.png' : '/images/company_blank.png';

?>
<div class="content-right-block p20">
    <div class="company">
        <div class="company-left">
            <img src="<?=$img_src?>" alt="" />
        </div>
        <div class="company-right">
            <div class="company-right-link">
                <a href="<?= Url::toRoute([
                    '/company/view',
                    'alias' => $model->alias,
                    'category' => $company_category->alias,
                    'subcategory' => $company_subcategory->alias,
                ]) ?>">
                    <?= CompanyType::findOne($model->type_id)->name . ' "'.$model->name.'"' ?>
                </a>
            </div>
            <div class="company-right-short-description"><?=$model->short_description?></div>
            <div class="company-right-checked <?=($model->checked) ? 'true' : 'false'?>">
                <div class="company-right-checked-icon"></div>
                <div class="company-right-checked-text"><?=($model->checked) ? 'проверено' : 'на проверке'?></div>
            </div>
            <div class="company-right-update">
                <a class="button-update" href="<?= Url::toRoute([
                    '/company/edit', 'user_id' => Yii::$app->user->id, 'id' => $model->id
                ]) ?>">Редактировать</a>
            </div>
        </div>
    </div>
</div>
