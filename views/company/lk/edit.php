<?php

/* @var $this yii\web\View */
/* @var $model app\models\Company */

$this->title = 'Редактирование компании / PosadObzor';
?>
<div class="content-right-title">Редактирование компании</div>
<?= $this->render('_form', [
    'model' => $model,
]) ?>
<?= $this->render('/company/_modal', ['model' => $model]) ?>
