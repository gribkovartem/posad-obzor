<?php

use yii\widgets\ListView;
use app\helpers\PluralHelper;
use yii\helpers\Url;
use app\models\CompanyCategory;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Все компании / PosadObzor';
$show_type = (isset($_COOKIE['show_type'])) ? $_COOKIE['show_type'] : '';
$small_active = ($show_type == 'small') ? 'active' : '';
$list_active = ($show_type == 'list' || $show_type == '') ? 'active' : '';
$category = (isset(Yii::$app->request->get()['category'])) ?
    CompanyCategory::findOne(['alias' => Yii::$app->request->get()['category']]) : '';
$subcategory = (isset(Yii::$app->request->get()['subcategory']) && Yii::$app->request->get()['subcategory'] != '') ?
    CompanyCategory::findOne(['alias' => Yii::$app->request->get()['subcategory']]) : '';

?>
<div class="modal" id="maps">
    <div class="modal-dialog">
        <div class="modal-close"></div>
        <div id="map" style="width: 600px; height: 400px;"></div>
    </div>
</div>
<div class="search-result">*Мы нашли для Вас <?=PluralHelper::pluralIt($dataProvider->getTotalCount(), [
        'компанию', 'компания', 'компании', 'компаний'
    ])?>.
</div>
<div class="company-breadcrumbs list">
    <div class="company-breadcrumbs-item"><a href="/">Главная</a></div>
    <div class="company-breadcrumbs-separator">/</div>
    <div class="company-breadcrumbs-item"><a href="<?=Url::toRoute(['/site/spravochnik'])?>">Справочник</a></div>
    <div class="company-breadcrumbs-separator">/</div>
    <?php if ($category != '' && $subcategory == ''): ?>
    <div class="company-breadcrumbs-item"><?=$category->name?></div>
    <?php else: ?>
    <div class="company-breadcrumbs-item"><a href="<?=
        Url::toRoute([
            '/company/list',
            'category' => $category->alias
        ])
        ?>"><?=$category->name?></a>
    </div>
    <?php endif; ?>
    <?php if ($subcategory != ''): ?>
    <div class="company-breadcrumbs-separator">/</div>
    <div class="company-breadcrumbs-item"><?=$subcategory->name?></div>
    <?php endif; ?>
</div>
<div class="show-types">
    <div class="view-type small <?=$small_active?>">
        <div class="view-type-icon">
            <div class="view-type-icon-item"></div>
            <div class="view-type-icon-item"></div>
            <div class="view-type-icon-item"></div>
            <div class="view-type-icon-item"></div>
        </div>
        <div class="view-type-text">Мелкие</div>
    </div>
    <div class="view-type list <?=$list_active?>">
        <div class="view-type-icon">
            <div class="view-type-icon-item"></div>
            <div class="view-type-icon-item"></div>
            <div class="view-type-icon-item"></div>
            <div class="view-type-icon-item"></div>
        </div>
        <div class="view-type-text">Список</div>
    </div>
</div>
<div class="companies-content">
    <div class="companies-block <?=$show_type?>">
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_view',
            'layout' => "{items}\n{pager}",
            'pager' => [
                'prevPageLabel' => '',
                'nextPageLabel' => '',
            ],
            'emptyText' => 'Ничего не найдено',
        ]); ?>
    </div>
    <div class="filter-block">
        <div class="filter-block-title">ПОДКАТЕГОРИЯ</div>
        <div class="filter-block-body">
            <?php foreach (CompanyCategory::find()->where(['parent_id' => $category->id])->all() as $key => $checkbox): ?>
                <?php $checked = ($subcategory != '' && $checkbox->alias == $subcategory->alias) ? 'checked' : '' ?>
                <div class="filter-block-body-checkbox" id="<?=$checkbox->id?>" data-category-id="<?=$checkbox->parent_id?>">
                    <input <?=$checked?> type="checkbox" class="checkbox <?=$checked?>" id="<?=$checkbox->alias?>" />
                    <label for="<?=$checkbox->alias?>"><?=$checkbox->name?></label>
                </div>
                <?php if ($key == 9): ?>
            <div class="filter-block-body-hidden">
                <?php endif; ?>
            <?php endforeach; ?>
            </div>
            <?php if (count(CompanyCategory::find()->where(['parent_id' => $category->id])->all()) > 10): ?>
                <div class="filter-block-body-more">Ещё</div>
            <?php endif; ?>
        </div>
    </div>
</div>
