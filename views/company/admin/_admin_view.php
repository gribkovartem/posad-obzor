<?php

use yii\helpers\Html;
use app\models\Company;

$background = ($model->checked) ? '#EAFF9E' : '#FFF090';
$background_name = (count(Company::find()->where(['alias' => $model->alias])->all()) > 1) ? '#FF6E6E' : false;
$background_name = ($background_name) ? $background_name : $background;

?>
<tr>
    <td style="background-color: <?=$background?>"><?= $model->id ?></td>
    <td style="background-color: <?=$background?>; text-align: center;"><img style="width: 60px;" src="/upload/company/<?= $model->id ?>/logo.png" alt=""/></td>
    <td style="background-color: <?=$background?>"><?= $model->type_id ?></td>
    <td style="background-color: <?=$background_name?>"><?= $model->name ?></td>
    <td style="background-color: <?=$background?>"><?= $model->short_description ?></td>
    <td style="background-color: <?=$background?>"><?= ($model->checked) ? 'да' : 'нет' ?></td>
    <td style="background-color: <?=$background?>">
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', array('/admin/default/update',
            'module' => 'company',
            'id' => $model->id)); ?>
    </td>
    <td style="background-color: <?=$background?>">
        <?= Html::a('<span class="glyphicon glyphicon-remove"></span>', array('/admin/default/delete',
            'module' => 'company',
            'id' => $model->id),array(
            'class' => 'delete',
        )); ?>
    </td>
</tr>