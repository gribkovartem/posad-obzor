<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;
use app\models\CompanyType;
use app\models\City;
use app\models\User;
use app\models\CompanyCompanyCategory;
use app\models\CompanyCategory;
use app\models\Company;

/* @var $this yii\web\View */
/* @var $model app\models\Company */
/* @var $form yii\widgets\ActiveForm */

$company_company_category = CompanyCompanyCategory::find()->where(['company_id' => $model->id])->all();
$company_company_category_new = new CompanyCompanyCategory();
$working_hours_from = ($model->id != '') ? json_decode($model->working_hours)->from : '';
$working_hours_to = ($model->id != '') ? json_decode($model->working_hours)->to : '';
$weekend = (($model->id != '')) ? json_decode($model->working_days) : '';

?>

<?php if (count(Company::find()->where(['alias' => $model->alias])->all()) > 1): ?>
    <?php if (!$model->checked): ?>
        <div style="font-weight: bold; color: #f00; margin: 20px 0">
            КОМПАНИЯ С ТАКИМ ЖЕ НАЗВАНИЕМ УЖЕ СУЩЕСТВУЕТ В БАЗЕ,
            УТОЧНИТЕ У ОТВЕТСТВЕННОГО ЛИЦА НАЗВАНИЕ И ЗАТЕМ СМЕНИТЕ АЛИАС
        </div>
    <?php endif; ?>
<?php endif; ?>

<?php $form = ActiveForm::begin([
    'options' => ['enctype'=>'multipart/form-data'],
    'enableAjaxValidation' => false,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'template' => "{label}\n{input}\n{error}",
    ],
]); ?>

<input type="hidden" name="company_id" />
<?php if (file_exists(Yii::getAlias('@webroot') . '/upload/company/'.$model->id.'/logo.png')): ?>
    <img id="logo-img" src="/upload/company/<?=$model->id?>/logo.png" alt="" />
<?php else: ?>
    <img id="logo-img" src="/images/company_blank.png" alt="" />
<?php endif; ?>
<?= $form->field($model, 'logo', ['template' => '{input}'])->fileInput(['id' => 'company-logo', 'class' => 'image-upload']) ?>

<?= $form->field($model, 'type_id', ['template' => '{label}{input}'])->dropDownList(
    ArrayHelper::map(CompanyType::find()->all(), 'id', 'name')
)?>
<?= $form->field($model, 'name')->textInput(['maxlength' => 255, 'placeholder' => 'InQuality']) ?>
<?= $form->field($model, 'alias')->textInput(['maxlength' => 255, 'placeholder' => 'in-quality']) ?>
<?= $form->field($model, 'short_description')->textInput(['maxlength' => 255, 'placeholder' => 'Веб разработка, дизайн']) ?>
<?= $form->field($model, 'description')->textarea(['rows' => 6, 'placeholder' => 'Написать подробное описание компании']) ?>

<label for="">Рубрики компании</label>
<div class="admin-rubric-block">
    <?php if ($company_company_category): ?>
        <?php foreach ($company_company_category as $category_key => $category): ?>
            <div class="rubric" id="<?=$category_key?>">
                <label for="">Рубрика #<?=$category_key + 1?></label>
                <?php if ($category_key > 0): ?>
                <span class="rubric-remove" data-rubric-id="<?=$category->id?>" style="color:#f00;float:right;cursor:pointer">Удалить</span>
                <?php endif; ?>
                <div class="form-group">
                    <?= $form->field($category, '['.$category_key.']category_id', ['template' => '{input}'])->dropDownList(
                        ArrayHelper::map(CompanyCategory::find()->where(['parent_id' => 0])->all(), 'id', 'name')
                    )?>
                    <?= $form->field($category, '['.$category_key.']subcategory_id', ['template' => '{input}'])->dropDownList(
                        ArrayHelper::map(CompanyCategory::find()
                            ->where('parent_id != 0')
                            ->all(), 'id', 'name')
                    )?>
                </div>
                <?= $form->field($category, '['.$category_key.']id', ['template' => '{input}'])->hiddenInput() ?>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <div class="rubric" id="">
            <label for="">Рубрика #1</label>
            <div class="form-group">
                <?= $form->field($company_company_category_new, '[0]category_id', ['template' => '{input}'])->dropDownList(
                    ArrayHelper::map(CompanyCategory::find()->where(['parent_id' => 0])->all(), 'id', 'name')
                )?>
                <?= $form->field($company_company_category_new, '[0]subcategory_id', ['template' => '{input}'])->dropDownList(
                    ArrayHelper::map(CompanyCategory::find()
                        ->where('parent_id != 0')
                        ->all(), 'id', 'name')
                )?>
            </div>
        </div>
    <?php endif; ?>
    <div class="append-rubric">Добавить еще одну</div>
</div>

<?= $form->field($model, 'city_id', ['template' => '{label}{input}'])->dropDownList(
    ArrayHelper::map(City::find()->all(), 'id', 'name')
) ?>
<?= $form->field($model, 'street')->textInput(['maxlength' => 255, 'placeholder' => 'Московское шоссе']) ?>
<?= $form->field($model, 'house')->textInput(['maxlength' => 255, 'placeholder' => '42']) ?>
<?= $form->field($model, 'office')->textInput(['maxlength' => 255, 'placeholder' => '604']) ?>
<?= $form->field($model, 'main_number')->textInput(['maxlength' => 255, 'class' => 'form-control phone-mask']) ?>
<?= $form->field($model, 'extra_number')->textInput(['maxlength' => 255, 'class' => 'form-control phone-mask']) ?>
<?= $form->field($model, 'site_url')->textInput(['maxlength' => 255, 'placeholder' => 'http://in-quality.ru',
    'class' => 'form-control http-mask']) ?>
<?= $form->field($model, 'email')->textInput(['maxlength' => 255, 'placeholder' => 'mail@in-quality.ru',
    'class' => 'form-control email-mask']) ?>

<div class="form-group">
    <label class="control-label" for="company-time-from">Часы работы с</label>
    <?= Html::textInput('', (!empty($working_hours_from)) ? $working_hours_from : '', [
        'class' => 'form-control from lcalendar_time', 'id' => 'company-time-from',
    ]) ?>
</div>
<div class="form-group">
    <label class="control-label" for="company-time-to">Часы работы до</label>
    <?= Html::textInput('', (!empty($working_hours_to)) ? $working_hours_to : '', [
        'class' => 'form-control to lcalendar_time', 'id' => 'company-time-to'
    ]) ?>
</div>
<div class="form-group">
    <label class="control-label" for="company-weekend">Выходные дни</label>
    <?= Html::dropDownList('', $weekend,
        [
            '1' => 'Понедельник',
            '2' => 'Вторник',
            '3' => 'Среда',
            '4' => 'Четверг',
            '5' => 'Пятница',
            '6' => 'Суббота',
            '7' => 'Воскресенье',
        ],
        ['class' => 'form-control', 'id' => 'company-weekend', 'multiple' => 'multiple']) ?>
</div>

<?= $form->field($model, 'vk')->textInput() ?>
<?= $form->field($model, 'fb')->textInput() ?>
<?= $form->field($model, 'tw')->textInput() ?>
<?= $form->field($model, 'inst')->textInput() ?>
<?= $form->field($model, 'ok')->textInput() ?>
<?= $form->field($model, 'mail')->textInput() ?>
<?= $form->field($model, 'c_user_id', ['template' => '{label}{input}'])->dropDownList(
    ArrayHelper::map(User::find()->all(), 'id', 'name')
)?>
<?= $form->field($model, 'checked')->checkbox() ?>

<?= $form->field($model, 'working_hours', ['template' => '{input}'])->hiddenInput() ?>
<?= $form->field($model, 'working_days', ['template' => '{input}'])->hiddenInput() ?>
<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>
