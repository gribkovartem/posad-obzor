<?php

use \app\models\Company;
use yii\widgets\ListView;

$module = (isset($_GET['module'])) ? $_GET['module'] : false;

?>

<div class="head">
    <h1><?= Company::RUS_NAME ?></h1>
</div>

<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>#</th>
        <th>Превью</th>
        <th>Тип</th>
        <th>Название</th>
        <th>Описание</th>
        <th>Проверена?</th>
        <th>Редактировать</th>
        <th>Удалить</th>
    </tr>
    </thead>
    <tbody>
    <?= ListView::widget([
        'dataProvider'=>$dataProvider,
        'itemView'=>'//company/admin/_admin_view',
    ]) ?>
    </tbody>
</table>