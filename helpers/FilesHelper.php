<?php

namespace app\helpers;

class FilesHelper
{

    public static function removeDirectory($path)
    {
        $files = glob($path . '/*');

        foreach ($files as $file) {
            is_dir($file) ? self::removeDirectory($file) : unlink($file);
        }

        rmdir($path);

        return;
    }

}