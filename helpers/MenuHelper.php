<?php

namespace app\helpers;

use \Yii;

class MenuHelper
{

    public static function isActive($type)
    {
        return ($type == Yii::$app->controller->id) ? 'class="active"' : '';
    }

}