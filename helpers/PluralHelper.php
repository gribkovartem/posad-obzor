<?php

namespace app\helpers;

use \Yii;

class PluralHelper
{

    public static function pluralIt($count, $words_array)
    {
        $return_value = '';
        $num = (iconv_strlen($count) >= 2) ? substr($count, -1) : $count;

        if ($count >= 11 && $count <= 14) {
            $return_value = $count . ' ' . $words_array[3];
        } else if ($count == 1) {
            $return_value = $count . ' ' . $words_array[0];
        } else if ($num == 1) {
            $return_value = $count . ' ' . $words_array[1];
        } else if ($num >= 2 && $num <= 4) {
            $return_value = $count . ' ' . $words_array[2];
        } else if ($num == 0 || ($num >= 5 && $num <= 9)) {
            $return_value = $count . ' ' . $words_array[3];
        }

        return $return_value;
    }

}