<?php

use yii\db\Schema;
use yii\db\Migration;

class m150219_134756_add_c_user_id_to_company extends Migration
{
    public function up()
    {
        $this->addColumn('company', 'c_user_id', 'int(11) UNSIGNED NOT NULL AFTER mail');
        $this->addForeignKey('c_user_id', 'company', 'c_user_id', 'user', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('c_user_id', 'company');
        $this->dropColumn('company', 'c_user_id');

        return false;
    }
}
