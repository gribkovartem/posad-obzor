<?php

use yii\db\Schema;
use yii\db\Migration;

class m150219_125211_create_city_table extends Migration
{
    public function up()
    {
        $this->createTable('city', [
            'id' => 'int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'name' => 'varchar(255) NOT NULL DEFAULT ""',
        ]);
    }

    public function down()
    {
        $this->dropTable('city');

        return false;
    }
}
