<?php

use yii\db\Schema;
use yii\db\Migration;

class m150312_104845_add_alias_to_companycategory extends Migration
{
    public function up()
    {
        $this->addColumn('company_category', 'alias', 'varchar(255) after `name`');
    }

    public function down()
    {
        $this->dropColumn('company_category', 'alias');

        return false;
    }
}
