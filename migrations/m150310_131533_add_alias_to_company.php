<?php

use yii\db\Schema;
use yii\db\Migration;

class m150310_131533_add_alias_to_company extends Migration
{
    public function up()
    {
        $this->addColumn('company', 'alias', 'varchar(255) after `name`');
    }

    public function down()
    {
        $this->dropColumn('company', 'alias');

        return false;
    }
}
