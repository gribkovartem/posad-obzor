<?php

use yii\db\Schema;
use yii\db\Migration;

class m150311_090922_add_lastname_and_patronymic_to_user extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'lastname', 'varchar(255) AFTER name');
        $this->addColumn('user', 'patronymic', 'varchar(255) AFTER lastname');
    }

    public function down()
    {
        $this->dropColumn('user', 'lastname');
        $this->dropColumn('user', 'patronymic');

        return false;
    }
}
