<?php

use yii\db\Schema;
use yii\db\Migration;

class m150225_135108_add_checked_to_companies extends Migration
{
    public function up()
    {
        $this->addColumn('company', 'checked', 'boolean AFTER c_user_id');
    }

    public function down()
    {
        $this->dropColumn('company', 'checked');

        return false;
    }
}
