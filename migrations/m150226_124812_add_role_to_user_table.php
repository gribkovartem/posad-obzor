<?php

use yii\db\Schema;
use yii\db\Migration;

class m150226_124812_add_role_to_user_table extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'role', 'varchar(255)');
    }

    public function down()
    {
        $this->dropColumn('user', 'role');

        return false;
    }
}
