<?php

use yii\db\Schema;
use yii\db\Migration;

class m150219_125138_create_company_type_table extends Migration
{
    public function up()
    {
        $this->createTable('company_type', [
            'id' => 'int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'name' => 'varchar(255) NOT NULL DEFAULT ""',
        ]);
    }

    public function down()
    {
        $this->dropTable('company_type');

        return false;
    }
}
