<?php

use yii\db\Schema;
use yii\db\Migration;

class m150217_131902_add_authkey_column_to_user extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'auth_key', 'varchar(255)');
    }

    public function down()
    {
        $this->dropColumn('user', 'auth_key');

        return false;
    }
}
