<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'js/vendors/lcalendar/lcalendar.css',
//        'js/vendors/mcustomscrollbar/jquery.mCustomScrollbar.min.css',
        'js/vendors/mcustomscrollbar/jquery.mCustomScrollbar.css',
        'css/main.css',
    ];
    public $js = [
        "http://api-maps.yandex.ru/2.1/?lang=ru_RU",
        'js/models/Slider.js',
        'js/models/Global.js',
        'js/models/Lk.js',
        'js/models/Company.js',
        'js/controllers/SliderController.js',
        'js/controllers/GlobalController.js',
        'js/controllers/LkController.js',
        'js/controllers/CompanyController.js',
        'js/vendors/lcalendar/lcalendar.js',
        'js/vendors/jquery-inputmask/dist/jquery.inputmask.bundle.min.js',
        'js/vendors/jquery-cookie/jquery.cookie.js',
        'js/vendors/mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js',
        'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
