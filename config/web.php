<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'name' => 'PosadObzor',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'DJz_i4PcFfvJlKWTcKiXmStQ05ujtt0R',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => [
                'user',
                'admin'
            ],
            'cache' => 'cache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['/'],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'showScriptName' => false,
            'rules' => [
                #site
                '/spravochnik' => '/site/spravochnik',
                #admin
                '/admin' => '/admin/default/index',
                '/admin/login' => '/admin/default/login',
                '/admin/logout' => '/admin/default/logout',
                '/admin/<module:\w+>' => '/admin/default/list',
                '/admin/<module:\w+>/seo' => '/admin/default/seo',
                '/admin/<module:\w+>/create' => '/admin/default/create',
                '/admin/<module:\w+>/upload' => '/admin/default/upload',
                '/admin/<module:\w+>/sendfile' => '/admin/default/sendfile',
                '/admin/<module:\w+>/uploadback' => '/admin/default/uploadback',
                '/admin/<module:\w+>/ajaxupdate' => '/admin/default/ajaxupdate',
                '/admin/<module:\w+>/<alias:[a-zA-Z0-9\-_]*>' => '/admin/default/view',
                '/admin/<module:\w+>/<alias:[a-zA-Z0-9\-_]*>/update' => '/admin/default/update',
                '/admin/<module:\w+>/<alias:[a-zA-Z0-9\-_]*>/delete' => '/admin/default/delete',
                '/admin/<module:\w+>/<id:\d+>/update' => '/admin/default/update',
                '/admin/<module:\w+>/<id:\d+>/delete' => '/admin/default/delete',
                #user
                '/user/<user_id:\d+>' => '/user/view',
                '/user/<user_id:\d+>/edit' => '/user/edit',
                '/user/<user_id:\d+>/update' => '/user/update',
                '/user/<user_id:\d+>/cp' => '/user/cp',
                '/user/<user_id:\d+>/uploadavatar' => '/user/uploadavatar',
                #company
                '/spravochnik/<category:[a-zA-Z0-9\-_]*>/<subcategory:[a-zA-Z0-9\-_]*>' => '/company/list',
                '/spravochnik/<category:[a-zA-Z0-9\-_]*>' => '/company/list',
                '/spravochnik/<category:[a-zA-Z0-9\-_]*>/<subcategory:[a-zA-Z0-9\-_]*>/<alias:[a-zA-Z0-9\-_]*>' =>
                    '/company/view',
                '/c/search' => '/company/search',
                '/company/<id:\d+>/upload/<type:\w+>' => '/company/upload',
                '/user/<user_id:\d+>/companies' => '/company/index',
                '/user/<user_id:\d+>/company/create' => '/company/create',
                '/user/<user_id:\d+>/company/<id:\d+>/edit' => '/company/edit',
                '/company/<id:\d+>/removeimage' => '/company/removeimage',
            ],
        ],
        'image' => array(
            'class' => 'yii\image\ImageDriver',
            'driver' => 'GD',
        ),
        'sphinx' => [
            'class' => 'yii\sphinx\Connection',
            'dsn' => 'mysql:host=127.0.0.1;port=9306;',
            'username' => '',
            'password' => '',
        ],
    ],
    'params' => $params,
    'modules' => [
        'gii' => [
            'class' => 'yii\gii\Module',
            'allowedIPs' => [
                $_SERVER['REMOTE_ADDR']
            ],
        ],
        'admin' => [
            'class' => 'app\modules\admin\Admin',
        ],
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

//    $config['bootstrap'][] = 'gii';
//    $config['modules']['gii'] = 'yii\gii\Module';
//    $config['modules']['gii']['allowedIPs'] = '*';
}

return $config;
