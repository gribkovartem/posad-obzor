<?php
use app\modules\admin\helpers\AMenuHelper;
?>
<?php foreach (AMenuHelper::getMenu() as $menu_item_key => $menu_item_value): ?>
    <li class="<?=AMenuHelper::isActiveModel($menu_item_value['en'], 'module')?>">
        <a href="/admin/<?=$menu_item_value['en']?>"><?= $menu_item_value['ru'] ?></a>
    </li>
<?php endforeach; ?>