<?php

namespace app\modules\admin\helpers;

use Yii;
use yii\helpers\FileHelper;

class AModelNameHelper
{
    public static function getModelName($module)
    {
        $models = $models = FileHelper::findFiles(Yii::getAlias('@app/models'));

        foreach ($models as $model) {
            $model = explode(DIRECTORY_SEPARATOR, $model);
            $model = end($model);
            $model = explode('.', $model)[0];

            if ($module == strtolower($model)) {
                return $model;
            }
        }

        return false;
    }
}