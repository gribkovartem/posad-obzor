<?php

namespace app\modules\admin;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'admin_module/css/main.css',
        'admin_module/bootstrap/css/bootstrap.min.css',
        'admin_module/summernote/summernote.css',
        'admin_module/bootstrap/css/datatables.bootstrap.css',
        'js/vendors/lcalendar/lcalendar.css',
    ];
    public $js = [
        'admin_module/bootstrap/js/bootstrap.min.js',
        'admin_module/ckeditor/ckeditor.js',
        'admin_module/summernote/summernote.min.js',
        'admin_module/summernote/lang/summernote-ru-RU.js',
        'admin_module/bootstrap/js/datatables.min.js',
        'admin_module/bootstrap/js/datatables.bootstrap.js',
        'js/models/Global.js',
        'js/controllers/GlobalController.js',
        'admin_module/js/main.js',
        'js/vendors/lcalendar/lcalendar.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
