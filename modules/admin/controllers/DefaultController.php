<?php

namespace app\modules\admin\controllers;

use app\controllers\CController;
use app\modules\admin\helpers\AModelNameHelper;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\web\HttpException;
use Yii;

class DefaultController extends CController
{
    public $layout = 'main';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionList($module)
    {
        $model = "\\app\\models\\" . AModelNameHelper::getModelName($module);

        $dataProvider = new ActiveDataProvider([
            'query' => $model::find()->orderBy('id DESC'),
            'pagination' => [
                'pageSize' => 0,
            ],
        ]);

        return $this->render('//'.$module.'/admin/list', [
            'dataProvider'=>$dataProvider,
        ]);
    }

    public function actionView($module, $alias)
    {
        return $this->render('//'.$module.'/admin/view', [
            'model'=>$this->loadModel($module, $alias),
        ]);
    }

    public function actionUpdate($module, $alias)
    {
        $model = "\\app\\models\\" . AModelNameHelper::getModelName($module);
        $model = $this->loadModel($model, $alias);

        if(isset($_POST[AModelNameHelper::getModelName($module)]))
        {
            $model->attributes = $_POST[AModelNameHelper::getModelName($module)];
            if (isset($model->e_date)) $model->e_date = date('Y-m-d H:i:s');
            $_alias = (isset($_POST[ucfirst($module)]['alias'])) ? $_POST[ucfirst($module)]['alias'] : $alias;

            if ($model->save()) {
                if (isset($_POST['apply'])) {
                    $this->redirect(['/admin/default/update', 'module' => $module, 'alias' => $_alias]);
                } else {
                    $this->redirect(['/admin/default/list', 'module' => $module]);
                }
            }
        }

        return $this->render('//'.$module.'/admin/update', [
            'model'=>$model,
        ]);
    }

    public function actionAjaxUpdate($module)
    {
        $this->render('application.views.'.$module.'.ajaxupdate');
    }

    public function actionCreate($module)
    {
        $model = "\\app\\models\\" . AModelNameHelper::getModelName($module);
        $model = new $model;

        if(isset($_POST[AModelNameHelper::getModelName($module)]))
        {
            $model->attributes = $_POST[AModelNameHelper::getModelName($module)];
            if (isset($model->c_date)) $model->c_date = date('Y-m-d H:i:s');
            if (isset($model->e_date)) $model->e_date = date('Y-m-d H:i:s');
            if ($model->save()) {
                if (isset($model->alias) || isset($model->id)) {
                    $this->redirect(['/admin/default/list', 'module' => $module]);
                }
            }
        }

        return $this->render('//'.$module.'/admin/create', [
            'module' => $module,
            'model' => $model,
        ]);
    }

    public function actionDelete($module, $alias)
    {
        $model = "\\app\\models\\" . AModelNameHelper::getModelName($module);

        if ($this->loadModel($model, $alias)->delete()) {
            $this->redirect(array('/admin/default/list', 'module' => $module));
        }
    }

//    public function actionSeo($module)
//    {
//        $model = Seo::model()->findByAttributes(array('module' => $module));
//
//        if(isset($_POST['Seo']))
//        {
//            $model->attributes = $_POST['Seo'];
//            $model->module = $module;
//            if ($model->save()) {
//                $this->redirect(array('/admin/default/list', 'module' => lcfirst($module)));
//            }
//        }
//
//        $this->render('application.views.'.$module.'.seo',array(
//            'module' => $module,
//            'model'=>$model,
//        ));
//    }

    public function loadModel($module, $alias)
    {
        $field = (is_numeric($alias)) ? 'id' : $field = 'alias';

        $model_name = ucfirst($module);
        $model = new $model_name;
        $model = $model->find()->where([$field => $alias])->one();

        if($model===null)
            throw new HttpException(404,'The requested page does not exist.');
        return $model;
    }
}
