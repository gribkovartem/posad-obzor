<?php

use yii\helpers\Html;
use app\modules\admin\AppAsset;
use app\modules\admin\widgets\MenuWidget;
use app\modules\admin\helpers\AMenuHelper;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>

    <?= Html::csrfMetaTags() ?>
    <title>Админка / <?= Html::encode(Yii::$app->name) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrapper">
    <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" target="_blank" href="/"><?= Yii::$app->name ?></a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="">
                        <a href="/admin"><span class="glyphicon glyphicon-home"></span> Главная</a>
                    </li>
                    <li class="">
                        <a href="/admin/<?=AMenuHelper::getUrlElement(2)?>/create">
                            <?php if (Yii::$app->request->get('module')): ?>
                            <span class="glyphicon glyphicon-plus"></span> Добавить
                            <?php endif; ?>
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a data-method="post" href="<?=Url::toRoute(['/site/logout'])?>">Выход</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="left-menu">
        <ul class="nav nav-pills nav-stacked">
            <?= MenuWidget::widget() ?>
        </ul>
    </div>
    <div class="content">
        <?= $content; ?>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
