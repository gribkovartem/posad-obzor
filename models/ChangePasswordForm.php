<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * CheckPasswordForm is the model to change user password.
 */
class ChangePasswordForm extends Model
{
    public $old_password;
    public $password;
    public $re_password;

    const RUS_NAME = false;
    const MENU_SORT = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['old_password', 'password', 're_password'], 'required', 'message' => 'Поле не может быть пустым'],
            ['re_password', 'compare', 'compareAttribute' => 'password', 'message' => 'Пароли должны совпадать!'],
            ['old_password', 'validatePassword'],
        ];
    }

    public function validatePassword($attribute)
    {
        $model = User::findOne(\Yii::$app->user->id);

        if (crypt($this->old_password, $model->salt) !== $model->password) {
            $this->addError($attribute, 'Неправильный старый пароль!');
        } else {
            return true;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'old_password' => 'Старый пароль:',
            'password' => 'Новый пароль:',
            're_password' => 'Повторите пароль:',
        ];
    }
}
