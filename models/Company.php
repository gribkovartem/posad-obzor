<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\models\CompanyCategory;
use app\models\CompanyCompanyCategory;
use yii\web\UploadedFile;

/**
 * This is the model class for table "company".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property integer $type_id
 * @property string $short_description
 * @property string $description
 * @property string $working_hours
 * @property string $working_days
 * @property integer $city_id
 * @property string $street
 * @property string $house
 * @property string $office
 * @property string $main_number
 * @property string $extra_number
 * @property string $site_url
 * @property string $email
 * @property string $vk
 * @property string $fb
 * @property string $tw
 * @property string $inst
 * @property string $ok
 * @property string $mail
 * @property integer $c_user_id
 * @property boolean $checked
 * @property string $c_date
 * @property string $e_date
 *
 * @property User $cUser
 * @property CompanyCategory $category
 * @property City $city
 * @property CompanyCategory $subcategory
 * @property CompanyType $type
 */
class Company extends ActiveRecord
{
    public $logo;
    public $gallery_image;

    const RUS_NAME = 'Компания';
    const MENU_SORT = 20;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'short_description', 'description', 'city', 'street', 'house', 'main_number'],
                'required', 'message' => 'Поле не может быть пустым'],
            [['type_id', 'city_id', 'c_user_id'], 'integer'],
            [['description'], 'string'],
            [['c_date', 'e_date', 'c_user_id', 'name'], 'safe'],
            [['name', 'alias', 'short_description', 'working_hours', 'working_days', 'street', 'house', 'office',
                'main_number', 'extra_number', 'site_url', 'email',
                'vk', 'fb', 'tw', 'inst', 'ok', 'mail'], 'string', 'max' => 255],
            [['logo', 'gallery_image'], 'file', 'extensions'=> ['png', 'jpg', 'jpeg']],
            [['checked'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название компании',
            'alias' => 'Алиас',
            'type_id' => 'Тип',
            'short_description' => 'Краткое описание',
            'description' => 'Полное описание',
            'working_hours' => 'Часы работы',
            'working_days' => 'Рабочие дни',
            'city_id' => 'Город',
            'street' => 'Улица',
            'house' => 'Дом',
            'office' => 'Офис',
            'main_number' => 'Телефон',
            'extra_number' => 'Дополнительный телефон',
            'site_url' => 'Адрес сайта',
            'email' => 'E-mail',
            'vk' => 'Вконтакте',
            'fb' => 'Фейсбук',
            'tw' => 'Твиттер',
            'inst' => 'Инстаграм',
            'ok' => 'Одноклассники',
            'mail' => 'Mail.ru',
            'c_user_id' => 'Ответственное лицо',
            'checked' => 'Проверена?',
            'c_date' => 'C Date',
            'e_date' => 'E Date',
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['edit'] = ['categories'];

        return $scenarios;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCUser()
    {
        return $this->hasOne(User::className(), ['id' => 'c_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(CompanyType::className(), ['id' => 'type_id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        //save company's categories
        foreach (Yii::$app->request->post()['CompanyCompanyCategory'] as $post) {
            $company_company_category = (isset($post['id'])) ?
                CompanyCompanyCategory::findOne($post['id']) : new CompanyCompanyCategory();
            $company_company_category->company_id = $this->primaryKey;
            $company_company_category->category_id = $post['category_id'];
            $company_company_category->subcategory_id = $post['subcategory_id'];
            $company_company_category->save();
        }

        //save company's logo
        if (!empty($_FILES['Company']['name']['logo'])) {
            $this->logo = UploadedFile::getInstance($this, 'logo');
            $id = $this->id;
            if ($this->validate(['logo'])) {
                if (!is_dir(Yii::getAlias('@webroot') . '/upload/company/'.$id)) {
                    mkdir(Yii::getAlias('@webroot') . '/upload/company/' . $id);
                }
                $this->logo->saveAs(Yii::getAlias('@webroot') . '/upload/company/' . $id . '/logo.png');
                $image = Yii::$app->image->load(Yii::getAlias('@webroot') . '/upload/company/' . $id . '/logo.png');
                $image->resize(120, false)->save();
            } else {
                return false;
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            CompanyCompanyCategory::deleteAll(['company_id' => Yii::$app->request->get()['alias']]);
            return true;
        } else {
            return false;
        }
    }

}
