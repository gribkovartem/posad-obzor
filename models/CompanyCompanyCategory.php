<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "company_company_category".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $category_id
 * @property integer $subcategory_id
 *
 * @property CompanyCategory $subcategory
 * @property CompanyCategory $category
 * @property Company $company
 */
class CompanyCompanyCategory extends ActiveRecord
{
    const RUS_NAME = false;
    const MENU_SORT = false;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_company_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'category_id', 'subcategory_id'], 'required'],
            [['company_id', 'category_id', 'subcategory_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'category_id' => 'Category ID',
            'subcategory_id' => 'Subcategory ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubcategory()
    {
        return $this->hasOne(CompanyCategory::className(), ['id' => 'subcategory_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(CompanyCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
