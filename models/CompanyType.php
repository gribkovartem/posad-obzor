<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "company_type".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Company[] $companies
 */
class CompanyType extends \yii\db\ActiveRecord
{
    const RUS_NAME = 'Тип компании';
    const MENU_SORT = 40;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['type_id' => 'id']);
    }
}
