$(document).ready(function() {
    SliderController.init();
    LkController.init();
    LCalendar.init('ru');
    GlobalController.init();
    CompanyController.init();

    $('.phone-mask').inputmask('phone', {
        url: "/js/vendors/jquery-inputmask/js/phone-codes/phone-codes.json"
    });
    $('.email-mask').inputmask({
        mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
        greedy: false,
        onBeforePaste: function (pastedValue, opts) {
            pastedValue = pastedValue.toLowerCase();
            return pastedValue.replace("mailto:", "");
        },
        definitions: {
            '*': {
                validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
                cardinality: 1,
                casing: "lower"
            }
        }
    });

    $("#scrollbar").mCustomScrollbar({
        setLeft: 0,
        axis: "x",
        autoHideScrollbar: false,
        contentTouchScroll: true,
        scrollInertia: 0
    });
});