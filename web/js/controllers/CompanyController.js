var CompanyController = {

    init: function() {
        Company.init();

        $('.show-types .view-type').click(function () {
            Company.toggleViewType($(this));
        });

        $('.show-map').click(function () {
            var address = '';
            var company = '';

            if ($(this).hasClass('company-list-right-address')) {
                address = $(this).data('city') + ' ' + $(this).find('span').text();
                company = $(this).parents('.company-list-right').find('.company-list-right-title a').text();
            } else {
                address = $('.company-view-info_block-contacts-item.address').find('span').last().text();
                company = $('.company-view-info_block-name').text();
            }

            Company.showMap(address);
        });

        $('.company-view-photo_block__dots-dot').click(function () {
            $('.company-view-photo_block__dots-dot').removeClass('active');
            $(this).addClass('active');
            Company.slideIt($(this).index());
        });

        $('.company-view-photo_block__gallery-item').click(function () {
            Company.showPhoto($(this));
        });

        $('.company-view-info_block-more').click(function () {
            Company.descriptionToggle();
        });

        $('.modal-arrow').click(function() {
           Company.changePhoto($(this).attr('class').split(' ')[$(this).attr('class').split(' ').length - 1]);
        });

        $('.modal#photo .modal-dialog .modal-image').click(function() {
           Company.changePhoto('right');
        });

        jQuery('body').keydown(function(e) {
            if (e.keyCode == 37) {
                Company.changePhoto('left');
            } else if(e.keyCode == 39) {
                Company.changePhoto('right');
            }
        });

        $('.filter-block-body-checkbox').click(function() {
            Company.filterCompanies();
        });

        $('.filter-block-body-more').click(function() {
            Company.filterToggle();
        });
    }
};