var SliderController = {

    init: function() {
        Slider.init(1000);

        left_arrow.click(function() {
           Slider.slideIt('left');
        });

        right_arrow.click(function() {
           Slider.slideIt('right');
        });

        $('.content-slider-dots-dot').click(function() {
           Slider.dotSlide($(this).index());
        });
    }

};