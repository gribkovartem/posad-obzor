var Global = {

    init: function() {
        //Global.initHeaderUnderMenu();
    },

    toggleTabs: function(tab_el, content_el, tab_index) {
        tab_el.removeClass('active');
        content_el.removeClass('active');

        tab_el.eq(tab_index).addClass('active');
        content_el.eq(tab_index).addClass('active');
    },

    toggleInputs: function() {
        $('.content-right-block-user-right').toggleClass('active');
    },

    startTabs: function(tab_el, content_el, tab_index) {
        tab_el.removeClass('active');
        content_el.removeClass('active');

        tab_el.eq(tab_index).addClass('active');
        content_el.eq(tab_index).addClass('active');
    },

    startModal: function() {
        if (window.location.hash == '#/signup/reg') {
            $('.modal').addClass('active');
            $('body').addClass('modaled');
        }
    },

    removeRubric: function(target) {
        var id = target.data('rubricId');
        var rubric_id = target.parents('.rubric').attr('id');

        if (confirm('Вы уверены?')) {
            $.ajax({
                type: 'post',
                url: '/company/deleterubric',
                data: {id: id},
                success: function(data) {
                    if (data == 1) {
                        target.parents('.rubric').remove();
                        $('#companycompanycategory-'+rubric_id+'-company_id').remove();
                        $('#companycompanycategory-'+rubric_id+'-category_id').remove();
                        $('#companycompanycategory-'+rubric_id+'-subcategory_id').remove();
                        $('#companycompanycategory-'+rubric_id+'-id').remove();
                    } else {
                        console.log('Произошла ошибка удаления рубрики');
                    }
                },
                dataType: 'text'
            });
        }
    },

    pluralIt: function(count, words_array) {
        var return_value = '';
        var num = (count.length >= 2) ? count.slice(-1) : count;

        if (count >= 11 && count <= 14) {
            return_value = count + ' ' + words_array[3];
        } else if (count == 1) {
            return_value = count + ' ' + words_array[0];
        } else if (num == 1) {
            return_value = count + ' ' + words_array[1];
        } else if (num >= 2 && num <= 4) {
            return_value = count + ' ' + words_array[2];
        } else if (num == 0 || (num >= 5 && num <= 9)) {
            return_value = count + ' ' + words_array[3];
        }

        return return_value;
    },

    initHeaderUnderMenu: function() {
        var width = 0;

        $('.header-under_menu-content-item').each(function() {
            width += $(this).outerWidth();
        });

        $('.header-under_menu-content').width(width);
    }

};