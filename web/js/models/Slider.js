var slider = $('.content-slider');
var slider_wrapper = slider.find('.content-slider-wrapper');
var slide = slider_wrapper.find('.content-slider-wrapper-slide');
var left_arrow = $('.content-slider-arrow-wrapper.left .content-slider-arrow');
var right_arrow = $('.content-slider-arrow-wrapper.right .content-slider-arrow');
var animation_time = 0;

var Slider = {

    init: function(animation_time_var) {
        Slider.initDots();
        Slider.initArrows();
        slider_wrapper.width((slide.length * slide.width()) + 10);
        animation_time = animation_time_var;
    },

    initDots: function() {
        for (var i = 1; i < slide.length; ++i) {
            $('.content-slider-dots').append('<div class="content-slider-dots-dot"></div>');
            $('.content-slider-dots-dot').eq(0).addClass('active');
        }
    },

    initArrows: function() {
        if (slide.length > 1) {
            right_arrow.addClass('opacity');
        }
    },

    changeDots: function(index) {
        $('.content-slider-dots-dot').removeClass('active');
        $('.content-slider-dots-dot').eq(index).addClass('active');
    },

    changeClassOfActiveSlide: function(index) {
        slide.removeClass('active');
        slide.eq(index).addClass('active');
    },

    setTranslateCrossBrowser: function(slider_wrapper_val, value) {
        slider_wrapper_val.css('transform', 'translateX('+value+'px)');
        slider_wrapper_val.css('-webkit-transform', 'translateX('+value+'px)');
        slider_wrapper_val.css('-moz-transform', 'translateX('+value+'px)');
        slider_wrapper_val.css('-o-transform', 'translateX('+value+'px)');
    },

    controlArrows: function() {
        if (slide.eq(0).hasClass('active')) {
            left_arrow.removeClass('opacity');
            right_arrow.addClass('opacity');
        } else if (slide.eq(slide.length - 1).hasClass('active')) {
            right_arrow.removeClass('opacity');
            left_arrow.addClass('opacity');
        } else {
            right_arrow.addClass('opacity');
            left_arrow.addClass('opacity');
        }
    },

    dotSlide: function(dot_index) {
        var wrapper_translate = slide.width() * dot_index;

        if (slider.hasClass('animated')) return false;
        slider.addClass('animated');

        Slider.setTranslateCrossBrowser(slider_wrapper, -wrapper_translate);
        Slider.changeClassOfActiveSlide(dot_index);
        Slider.changeDots(dot_index);
        Slider.controlArrows();

        setTimeout(function(){slider.removeClass('animated')}, animation_time);
    },

    slideIt: function(direction) {
        var transform = slider_wrapper.css("transform").split(',');
        var translate = parseInt(transform[4]);
        var active_slide_index = slider_wrapper.find('.content-slider-wrapper-slide.active').index();

        if (slider.hasClass('animated')) return false;
        slider.addClass('animated');

        if (direction === 'left') {
            if (translate !== 0) {
                Slider.setTranslateCrossBrowser(slider_wrapper, translate + slide.width());
                Slider.changeClassOfActiveSlide(active_slide_index - 1);
                Slider.changeDots(active_slide_index - 1);
            }
        } else if (direction === 'right') {

            if ((active_slide_index + 1) < slide.length) {
                Slider.setTranslateCrossBrowser(slider_wrapper, translate - slide.width());
                Slider.changeClassOfActiveSlide(active_slide_index + 1);
                Slider.changeDots(active_slide_index + 1);
            }
        }

        Slider.controlArrows();

        setTimeout(function(){slider.removeClass('animated')}, animation_time);
    }

};