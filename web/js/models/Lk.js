var Lk = {

    init: function() {

    },

    toggleModal: function(modal_id) {
        $('body').addClass('modaled');
        $('#'+modal_id).addClass('active');
    },

    insertCategory: function(target) {
        var parent_category_id = target.data('id');
        var categories = $('.modal#category .categories');
        var rubric_id = $('.modal.active#category').attr('data-rubric-id');

        categories.eq(0).removeClass('active');
        categories.eq(1).addClass('active');

        categories.eq(1).find('.category').hide();
        categories.eq(1).find('.category[data-parent-id="'+parent_category_id+'"]').show();

        categories.eq(1).find('input').attr('data-parent-id', parent_category_id);

        $('.rubric#'+rubric_id+' input').val(target.text());
        $('#companycompanycategory-'+rubric_id+'-category_id').val(parent_category_id);
    },

    insertSubCategory: function(target) {
        var subcategory_id = target.data('id');
        var categories = $('.modal#category .categories');
        var rubric_id = $('.modal.active#category').attr('data-rubric-id');

        $('.rubric#'+rubric_id+' input').val(
            $('.rubric#'+rubric_id+' input').val() + ' -> ' + $.trim(target.text())
        );
        $('#companycompanycategory-'+rubric_id+'-subcategory_id').val(subcategory_id);

        $('body').removeClass('modaled');
        $('.modal').removeClass('active');
        categories.eq(1).removeClass('active');
        categories.eq(0).addClass('active');
    },

    searchCategory: function(target) {
        var input_value = target.val();
        var categories = $('.modal#category .categories').find('.category');
        var parent_id = target.parents('.modal-dialog').find('.categories').eq(1).find('input').attr('data-parent-id');

        categories.hide();

        categories.each(function(index) {
            if (categories.eq(index).text().toLowerCase().indexOf(input_value.toLowerCase()) >= 0) {
                categories.eq(index).show();
                if ($('.modal#category .categories').last().hasClass('active')) {
                    if (parent_id != categories.eq(index).data('parentId')) {
                        categories.eq(index).hide();
                    }
                }
            }
        });
    },

    appendRubric: function(target) {
        var rubric = target.prev().clone().attr('id', parseInt(target.prev().attr('id')) + 1);
        rubric.find('input').val('');

        var hidden_company = $('.field-companycompanycategory-0-company_id').find('input:last-of-type').clone();
        var hidden_company_id = hidden_company.attr('id').split('-')[1];
        hidden_company.attr('id', 'companycompanycategory-'+(parseInt(hidden_company_id) + 1)+'-company_id');
        hidden_company.attr('name', 'CompanyCompanyCategory[' + (parseInt(hidden_company_id) + 1) + '][company_id]');
        $('.field-companycompanycategory-0-company_id').append(hidden_company);

        var hidden_category = $('.field-companycompanycategory-0-category_id').find('input:last-of-type').clone();
        var hidden_category_id = hidden_category.attr('id').split('-')[1];
        hidden_category.attr('id', 'companycompanycategory-'+(parseInt(hidden_category_id) + 1)+'-category_id');
        hidden_category.attr('name', 'CompanyCompanyCategory[' + (parseInt(hidden_category_id) + 1) + '][category_id]');
        $('.field-companycompanycategory-0-category_id').append(hidden_category);

        var hidden_subcategory = $('.field-companycompanycategory-0-subcategory_id').find('input:last-of-type').clone();
        var hidden_subcategory_id = hidden_subcategory.attr('id').split('-')[1];
        hidden_subcategory.attr('id', 'companycompanycategory-'+(parseInt(hidden_subcategory_id) + 1)+'-subcategory_id');
        hidden_subcategory.attr('name', 'CompanyCompanyCategory[' + (parseInt(hidden_subcategory_id) + 1) + '][subcategory_id]');
        $('.field-companycompanycategory-0-subcategory_id').append(hidden_subcategory);

        target.before(rubric);
    },

    insertWorkingHours: function() {
        var from = $('.content-right-block-company-mode .content-right-block-company-mode__item').find('input.from').val();
        var to = $('.content-right-block-company-mode .content-right-block-company-mode__item').find('input.to').val();
        var working_hours_json = '{"from": "'+from+'", "to": "'+to+'"}';

        $('#company-working_hours').val(working_hours_json);
    },

    insertWorkingDays: function() {
        var days_json = [];
        var days_words = [];
        var working_days = $('.working-days-block');
        var working_day = working_days.find('.working-days-block-day');

        working_day.each(function(index) {
            if (working_day.eq(index).find('.checkbox').hasClass('checked')) {
                days_json.push(working_day.eq(index).find('.checkbox').attr('id'));
            }
        });

        days_json.forEach(function(index) {
            if (index == 1) days_words.push('понедельник');
            if (index == 2) days_words.push('вторник');
            if (index == 3) days_words.push('среда');
            if (index == 4) days_words.push('четверг');
            if (index == 5) days_words.push('пятница');
            if (index == 6) days_words.push('суббота');
            if (index == 7) days_words.push('воскресенье');
        });

        $('.content-right-block-company-mode__item input#weekend').val(days_words);
        $('#company-working_days').val(JSON.stringify(days_json));
    },

    insertSocial: function(target) {
        target.parents('.content-right-block-company-social__item').find('.form-group:last-of-type').find('input')
            .attr('name', 'Company['+target.val()+']');
    },

    startLoadAnimation: function() {
        $('.content-right-block-company-gallery__item-previews').attr('data-upload-count',
            $("#company-gallery_image:file")[0].files.length);
        $('.content-right-block-company-gallery__item-previews').attr('data-total-count',
            $("#company-gallery_image:file")[0].files.length +
            parseInt($('.content-right-block-company-gallery__item-previews').attr('data-images-count')));
        $('.content-right-block-company-gallery__item-previews').hide();
        $('.content-right-block-company-gallery__item-load').show();
    },

    removeGalleryImage: function(target) {
        var company_id = target.parents('article').data('companyId');
        var image_id = target.next('img').attr('id');

        if (confirm('Вы уверены?')) {
            $.ajax({
                type: 'post',
                url: '/company/'+company_id+'/removeimage',
                data: {image_id: image_id},
                success: function() {
                    var images_count =
                        parseInt($(".content-right-block-company-gallery__item-previews").attr("data-images-count")) - 1;

                    target.parents('article').remove();
                    $(".content-right-block-company-gallery__item-controls-error").removeClass("active");
                    $(".content-right-block-company-gallery__item-previews").attr("data-images-count", images_count);
                }
            });
        }
    }

};