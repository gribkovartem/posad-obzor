var Company = {

    init: function() {
        Company.initMap();
        Company.initSlider();
    },

    toggleViewType: function(target) {
        var type = target.attr('class').split(' ')[1];

        $('.show-types .view-type').removeClass('active');
        $('.show-types .view-type.'+type).addClass('active');

        $('.companies-block').removeClass('list small').addClass(type);
        $.cookie('show_type', type);
    },

    initMap: function() {
        ymaps.ready(init);
        var myMap,
            myPlacemark;

        function init() {
            var geocoder = ymaps.geocode($('.company-view-info_block-contacts-item.address').find('span').last().text());
            var coordinates = '';
            var company_name = $('.company-view-info_block-name').text();

            geocoder.then(
                function (res) {
                    coordinates = res.geoObjects.get(0).geometry.getCoordinates();

                    myMap = new ymaps.Map("static-map", {
                        center: coordinates,
                        zoom: 16,
                        controls: ["zoomControl"]
                    });

                    myPlacemark = new ymaps.Placemark(coordinates, {
                        hintContent: company_name
                    });

                    myMap.geoObjects.add(myPlacemark);
                },
                function (err) {
                    console.log('Ошибка');
                }
            );
        }
    },

    showMap: function(address, company) {
        $('.modal#maps #map').html('');
        $('.modal#maps').addClass('active');
        $('body').addClass('modaled');

        ymaps.ready(init);
        var myMap,
            myPlacemark;

        function init() {
            var geocoder = ymaps.geocode(address);
            var coordinates = '';
            var company_name = company;

            geocoder.then(
                function (res) {
                    coordinates = res.geoObjects.get(0).geometry.getCoordinates();

                    myMap = new ymaps.Map("map", {
                        center: coordinates,
                        zoom: 15,
                        controls: ["zoomControl"]
                    });

                    myPlacemark = new ymaps.Placemark(coordinates, {
                        hintContent: company_name
                    });

                    myMap.geoObjects.add(myPlacemark);
                },
                function (err) {
                    console.log('Ошибка');
                }
            );
        }
    },

    initSlider: function() {
        var slider_dots = $('.company-view-photo_block__dots');
        var slider_items = $('.company-view-photo_block__gallery-wrapper .company-view-photo_block__gallery-item');
        var slider_list_count = Math.ceil(slider_items.length / 3);

        for (var i = 0; i < slider_list_count; i++) {
            if (i == 0) {
                slider_dots.append('<div class="company-view-photo_block__dots-dot active"></div>');
            } else {
                slider_dots.append('<div class="company-view-photo_block__dots-dot"></div>');
            }
        }
    },

    slideIt: function(dot_index) {
        Slider.setTranslateCrossBrowser($('.company-view-photo_block__gallery-wrapper'), dot_index * -262);
    },

    showPhoto: function(target) {
        $('.company-view-photo_block__gallery-item').removeClass('active');
        target.addClass('active');
        $('.modal#photo .modal-dialog .modal-image').html(target.find('img').last().clone());

        $('.modal#photo').addClass('active');
        $('body').addClass('modaled');
    },

    changePhoto: function(direction) {
        var active_image_index = $('.company-view-photo_block__gallery-item.active').index();
        $('.company-view-photo_block__gallery-item').removeClass('active');

        if (direction == 'left') {
            var prev_img = $('.company-view-photo_block__gallery-item').eq(active_image_index - 1)
                .addClass('active').find('img').last().clone();
            $('.modal#photo .modal-dialog .modal-image').html(prev_img);
        } else if (direction == 'right') {
            if ((active_image_index + 1) < $('.company-view-photo_block__gallery-item').length) {
                var next_img = $('.company-view-photo_block__gallery-item').eq(active_image_index + 1)
                    .addClass('active').find('img').last().clone();
                $('.modal#photo .modal-dialog .modal-image').html(next_img);
            } else {
                var next_img = $('.company-view-photo_block__gallery-item').eq(0)
                    .addClass('active').find('img').last().clone();
                $('.modal#photo .modal-dialog .modal-image').html(next_img);
            }
        }
    },

    filterCompanies: function() {
        var checkbox = $('.filter-block-body-checkbox');
        var category_id = checkbox.data('categoryId');
        var subcategories = [];

        checkbox.each(function(index) {
            if (checkbox.eq(index).find('input').hasClass('checked')) {
                subcategories.push(checkbox.eq(index).attr('id'));
            }
        });

        $.ajax({
            method: "POST",
            url: "/company/ajax",
            data: { category_id: category_id, subcategories: subcategories },
            type: 'html'
        }).done(function(data) {
            $('.companies-block').html(data);
            var filtered_companies = $('.company-list').length;
            var word = Global.pluralIt(filtered_companies, ['компанию', 'компания', 'компании', 'компаний']);

            $('.search-result').text('*Мы нашли для Вас '+word+'.');
        });
    },

    filterToggle: function() {
        var height = $('.filter-block-body-hidden .filter-block-body-checkbox').length * (20 + 5);
        var filter = $('.filter-block-body');

        filter.toggleClass('expand');

        if (filter.hasClass('expand')) {
            $('.filter-block-body-hidden').height(height);
            $('.filter-block-body-more').text('Меньше');
        } else {
            $('.filter-block-body-hidden').height(0);
            $('.filter-block-body-more').text('Ещё');
        }
    },

    descriptionToggle: function() {
        var description = $('.company-view-info_block-description');

        description.toggleClass('more');

        if (description.hasClass('more')) {
            $('.company-view-info_block-more').text('показать меньше');
        } else {
            $('.company-view-info_block-more').text('показать еще');
        }
    }

};