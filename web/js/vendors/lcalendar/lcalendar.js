/**
 * Add daysInMonth method, to know count of days in month
 *
 * @returns {number}
 */
Date.prototype.daysInMonth = function() {
    return 32 - new Date(this.getFullYear(), this.getMonth(), 32).getDate();
};

/**
 * LCalendar class description
 *
 * @type {{init: init, makeStructure: makeStructure, translate: translate}}
 */
var LCalendar = {

    /**
     * Initialize lcalendar
     */
    init: function(lang) {
        var date = new Date();
        var v_date = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);

        $('.lcalendar_date').each(function(target) {
            LCalendar.makeStructure(target, lang, v_date, false);
        });

        $('.lcalendar_time').each(function(target) {
            LCalendar.makeTimeStructure(target);
        });
    },

    /**
     * Control clicks of time picker controls
     * @param target
     */
    controlTimeActions: function(target) {
        var timepicker = $('.lcalendar.time').eq(target);

        var h_ribbon = timepicker.find('.lcalendar-body-ribbon.hour');
        var m_ribbon = timepicker.find('.lcalendar-body-ribbon.minute');

        var h_up = timepicker.find('.lcalendar-head-hour-up');
        var h_down = timepicker.find('.lcalendar-footer-hour-down');

        var m_up = timepicker.find('.lcalendar-head-time-up');
        var m_down = timepicker.find('.lcalendar-footer-time-down');

        h_up.on('click', function() {
            var prev_hour = parseInt(h_ribbon.find('div:first').text());
            prev_hour = (prev_hour == 0) ? 23 : prev_hour - 1;

            h_ribbon.find('.active').removeAttr('class').prev().addClass('active');
            h_ribbon.find('div:last').remove();
            h_ribbon.prepend('<div>'+('0' + prev_hour).slice(-2)+'</div>');
        });

        h_down.on('click', function() {
            var next_hour = parseInt(h_ribbon.find('div:last').text());
            next_hour = (next_hour == 23) ? 0 : next_hour + 1;

            h_ribbon.find('.active').removeAttr('class').next().addClass('active');
            h_ribbon.find('div:first').remove();
            h_ribbon.append('<div>'+('0' + next_hour).slice(-2)+'</div>');
        });

        m_up.on('click', function() {
            var prev_minute = parseInt(m_ribbon.find('div:first').text());
            prev_minute = (prev_minute == 0) ? 55 : prev_minute - 5;

            m_ribbon.find('.active').removeAttr('class').prev().addClass('active');
            m_ribbon.find('div:last').remove();
            m_ribbon.prepend('<div>'+('0' + prev_minute).slice(-2)+'</div>');
        });

        m_down.on('click', function() {
            var next_minute = parseInt(m_ribbon.find('div:last').text());
            next_minute = (next_minute == 55) ? 0 : next_minute + 5;

            m_ribbon.find('.active').removeAttr('class').next().addClass('active');
            m_ribbon.find('div:first').remove();
            m_ribbon.append('<div>'+('0' + next_minute).slice(-2)+'</div>');
        });
    },

    /**
     * Processing all clicks for calendar
     */
    controlActions: function(index, target) {
        target.click(function() {
            $('.lcalendar').removeClass('active');
            target.next('.lcalendar').toggleClass('active');
        });

        $('body').click(function() {
            if ($('.lcalendar:hover').length <= 0 &&
                $('.lcalendar_date:hover').length <= 0 &&
                $('.lcalendar_time:hover').length <= 0) {
                $('.lcalendar').removeClass('active');
            }
        });

        target.next('.lcalendar').find('.lcalendar-body tbody tr td').click(function() {
            LCalendar.toggleDate(index, $(this));
        });

        $('.lcalendar.time').eq(index).click(function() {
            $('.lcalendar_time').eq(index).val($(this).find('.lcalendar-body-ribbon.hour').find('div.active').text()+
                ':'+$(this).find('.lcalendar-body-ribbon.minute').find('div.active').text());
        });
    },

    /**
     * Make time picker skeleton
     * @param target
     */
    makeTimeStructure: function(target) {
        var calendar_time = $('.lcalendar_time').eq(target);

        calendar_time.after(
            '<div class="lcalendar time">' +
                '<div class="lcalendar-triangle"></div>' +
                '<div class="lcalendar-head">' +
                    '<div class="lcalendar-head-hour-up control"></div>' +
                    '<div class="lcalendar-head-time-up control"></div>' +
                '</div>' +
                '<div class="lcalendar-body">' +
                    '<hr/>' +
                    '<hr/>' +
                    '<div class="lcalendar-body-ribbon hour">' +
                        '<div>23</div>' +
                        '<div class="active">00</div>' +
                        '<div>01</div>' +
                    '</div>' +
                    '<div class="lcalendar-body-ribbon minute">' +
                        '<div>55</div>' +
                        '<div class="active">00</div>' +
                        '<div>05</div>' +
                    '</div>' +
                '</div>' +
                '<div class="lcalendar-footer">' +
                    '<div class="lcalendar-footer-hour-down control down"></div>' +
                    '<div class="lcalendar-footer-time-down control down"></div>' +
                '</div>' +
            '</div>'
        );

        LCalendar.controlActions(target, calendar_time);
        LCalendar.controlTimeActions(target);
    },

    /**
     * Make calendar skeleton
     */
    makeStructure: function(target, lang, date, replace) {
        date = date.split('-');
        var date_month = ('0' + date[1]).slice(-2);
        var date_day = ('0' + date[2]).slice(-2);
        date = date[0]+'-'+date_month+'-'+date_day;
        var calendar_date = $('.lcalendar_date').eq(target);
        var today = new Date(date);
        var c_day = today.getDate();
        var c_month = today.getMonth();
        var c_month_real = c_month + 1;
        var c_year = today.getFullYear();
        var c_days_in_month = today.daysInMonth();
        var thead = [];
        
        for (var x = 0; x <= 6; ++x) { thead[thead.length] = '<th>'+LCalendar.translate(lang, 'd', x)+'</th>'; }

        if (replace == false) {
            calendar_date.after(
                '<div class="lcalendar">' +
                    '<div class="lcalendar-triangle"></div>' +
                    '<div class="lcalendar-head">' +
                        '<div class="lcalendar-head-prev control"></div>' +
                        '<div class="lcalendar-head-center">' +
                        '<div data-month="'+c_month_real+'" class="lcalendar-head-month">'+LCalendar.translate(lang, 'm', c_month)+'</div>' +
                        '<div data-year="'+c_year+'" class="lcalendar-head-year">'+c_year+'</div>' +
                        '</div>' +
                        '<div class="lcalendar-head-next control"></div>' +
                    '</div>' +
                    '<div class="lcalendar-body">' +
                        '<table>' +
                            '<thead>' + '<tr>' +
                                thead.join('') +
                            '</tr>' + '</thead>' +
                            '<tbody></tbody>' +
                        '</table>' +
                    '</div>' +
                '</div>'
            );
        } else {
            calendar_date.next('.lcalendar:first').empty().append(
                '<div class="lcalendar-triangle"></div>' +
                '<div class="lcalendar-head">' +
                    '<div class="lcalendar-head-prev control"></div>' +
                    '<div class="lcalendar-head-center">' +
                    '<div data-month="'+c_month_real+'" class="lcalendar-head-month">'+LCalendar.translate(lang, 'm', c_month)+'</div>' +
                    '<div data-year="'+c_year+'" class="lcalendar-head-year">'+c_year+'</div>' +
                    '</div>' +
                    '<div class="lcalendar-head-next control"></div>' +
                '</div>' +
                '<div class="lcalendar-body">' +
                    '<table>' +
                        '<thead>' + '<tr>' +
                            thead.join('') +
                        '</tr>' + '</thead>' +
                        '<tbody></tbody>' +
                    '</table>' +
                '</div>'
            );
        }

        var calendar = calendar_date.next('.lcalendar:not(time)');

        for (var i = 1; i <= c_days_in_month; i++) {
            var dow = new Date(c_year + '-' + ('0' + c_month_real).slice(-2) + '-' + ('0' + i).slice(-2)).getDay();

            if (i !== c_day) {
                calendar.find('.lcalendar-body tbody').append('<td data-dow="'+dow+'">'+i+'</td>');
            } else {
                calendar.find('.lcalendar-body tbody').append('<td data-dow="'+dow+'" class="current">'+i+'</td>');
            }
        }

        while (calendar.find('.lcalendar-body tbody td').eq(0).data('dow') > 0) {
            var prev_dow = calendar.find('.lcalendar-body tbody td').eq(0).data('dow') - 1;
            calendar.find('.lcalendar-body tbody').prepend('<td data-dow="'+prev_dow+'"></td>');
        }

        while (calendar.find('.lcalendar-body tbody td').eq(calendar.find('.lcalendar-body tbody td').length - 1).data('dow') < 6) {
            var next_dow = calendar.find('.lcalendar-body tbody td').eq(calendar.find('.lcalendar-body tbody td').length - 1).data('dow') + 1;
            calendar.find('.lcalendar-body tbody').append('<td data-dow="'+next_dow+'"></td>');
        }

        var body_td = calendar.find('.lcalendar-body tbody td');

        for (var z = 0; z < body_td.length; ++z) {
            if (body_td.eq(z).data('dow') == 0) {
                body_td.eq(z).addClass('sow');
            } else if (body_td.eq(z).data('dow') == 6) {
                body_td.eq(z).addClass('eow');
            }
        }

        calendar.find('.lcalendar-body tbody .sow').each(function() {
            $(this).nextUntil('.eow').andSelf().wrapAll("<tr></tr>");
        });

        calendar.find('.lcalendar-body tbody .eow').each(function() {
            $(this).prev('tr').append($(this));
        });

        calendar.find('.lcalendar-head-prev').click(function() {
            LCalendar.changeMonth($(this), target, lang, 'prev');
        });

        calendar.find('.lcalendar-head-next').click(function() {
            LCalendar.changeMonth($(this), target, lang, 'next');
        });

        LCalendar.controlActions(target, calendar_date);
    },

    /**
     * Change active date and put it into input
     *
     * @param index
     * @param target
     */
    toggleDate: function(index, target) {
        $('.lcalendar-body tbody td').removeClass('current');
        $(target).addClass('current');

        var date = ('0' + $('.current').text()).slice(-2) + '.' +
            ('0' + ($('.lcalendar-head-month').eq(index).data('month'))).slice(-2) + '.' +
            $('.lcalendar-head-year').eq(index).data('year');

        $('.lcalendar_date').eq(index).val(date);
    },

    /**
     * Change month and year for clicking the nav buttons
     *
     * @param target
     * @param lang
     * @param direction
     */
    changeMonth: function(target, index, lang, direction) {
        var lcalendar = target.parents('.lcalendar');
        var day = ('0' + new Date().getDate()).slice(-2);
        var year = lcalendar.find('.lcalendar-head-year').data('year');
        var next_year = year + 1;
        var prev_year = year - 1;
        var month = lcalendar.find('.lcalendar-head-month').data('month');
        var next_month = ('0' + (month + 1)).slice(-2);
        var prev_month = ('0' + (month - 1)).slice(-2);

        if (direction == 'next') {
            if (next_month == 13) {
                next_month = 1;
                year = next_year;
            }

            LCalendar.makeStructure(index, lang, year+'-'+next_month+'-'+day, true);
        } else {
            if (prev_month == 0) {
                prev_month = 12;
                year = prev_year;
            }

            LCalendar.makeStructure(index, lang, year+'-'+prev_month+'-'+day, true);
        }
    },

    /**
     * You can append here your language ;)
     *
     * @param lang
     * @param type
     * @param number
     * @returns {*}
     */
    translate: function(lang, type, number) {
        var months = {
            "en": ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            "ru": ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            "ukr": ['Сiчень', 'Лютий', 'Березень', 'Квiтень', 'Травень', 'Червень', 'Липень', 'Серпень', 'Вересень', 'Жовтень', 'Листопад', 'Грудень']
        };

        var days = {
            "en": ['TU', 'WE', 'TH', 'FR', 'SA', 'SU', 'MO'],
            "ru": ['ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ'],
            "ukr": ['НД', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ']
        };

        if (type === 'm') {
            return months[lang][number];
        } else if (type === 'd') {
            return days[lang][number];
        } else {
            return false;
        }
    }

};