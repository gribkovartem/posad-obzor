var AdminController = {

    init: function() {
        $('input.title-field').on('input', function() {
            Admin.makeAlias($(this).val());
        });

        $('.delete').click(function() {
            return confirm('Вы уверены?');
        });

        $('#back_submit').click(function() {
            Admin.uploadBackImage();
        });

        $('.price .accept').click(function() {
            Admin.acceptPrice($(this));
        });

        $('.append_service_info_block').click(function() {
            Admin.appendServiceInfoBlock($(this));
        });

        $('.remove_service_info_block').click(function() {
           Admin.removeServiceInfoBlock($(this).parents('form').find('.info_block input:last').val());
        });

        $('.append-rubric').click(function() {
           Admin.appendRubric($(this));
        });

        $('body').click(function() {
           Admin.insertWorkingHours();
        });

        $('#company-weekend').change(function() {
           Admin.insertWorkingDays();
        });
    }

};