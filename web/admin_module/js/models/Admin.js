var Admin = {

    init: function() {

    },

    makeAlias: function(title) {
        var abc = new Array();

        abc['а']='a';
        abc['б']='b';
        abc['в']='v';
        abc['г']='g';
        abc['д']='d';
        abc['е']='e';
        abc['ё']='yo';
        abc['ж']='j';
        abc['з']='z';
        abc['и']='i';
        abc['й']='y';
        abc['к']='k';
        abc['л']='l';
        abc['м']='m';
        abc['н']='n';
        abc['о']='o';
        abc['п']='p';
        abc['р']='r';
        abc['с']='s';
        abc['т']='t';
        abc['у']='u';
        abc['ф']='f';
        abc['х']='h';
        abc['ц']='c';
        abc['ч']='ch';
        abc['ш']='sh';
        abc['щ']='sh';
        abc['ы']='i';
        abc['э']='e';
        abc['ю']='yu';
        abc['я']='ya';

        abc['a']='a';
        abc['b']='b';
        abc['c']='c';
        abc['d']='d';
        abc['e']='e';
        abc['f']='f';
        abc['g']='g';
        abc['h']='h';
        abc['i']='i';
        abc['j']='j';
        abc['k']='k';
        abc['l']='l';
        abc['m']='m';
        abc['n']='n';
        abc['o']='o';
        abc['p']='p';
        abc['q']='q';
        abc['r']='r';
        abc['s']='s';
        abc['t']='t';
        abc['u']='u';
        abc['v']='v';
        abc['w']='w';
        abc['x']='x';
        abc['y']='y';
        abc['z']='z';

        abc['1']='1';
        abc['2']='2';
        abc['3']='3';
        abc['4']='4';
        abc['5']='5';
        abc['6']='6';
        abc['7']='7';
        abc['8']='8';
        abc['9']='9';
        abc['0']='0';

        abc['-']='-';
        abc[' ']='-';
        abc['_']='-';
        abc['.']='-';
        abc[',']='-';

        var txt=title.toLowerCase();
        var txtnew='';
        var finaltxt='';
        var symb='';

        for (kk=0;kk<txt.length;kk++) {
            symb=abc[txt.substr(kk,1)]?abc[txt.substr(kk,1)]:'';
            txtnew=txtnew.substr(0,txtnew.length)+symb;
        }

        for (kk=0;kk<txtnew.length;kk++) {
            symb=txtnew.substr(kk,1);
            if (symb=='-' && txtnew.substr((kk+1),1)=='-') continue;
            finaltxt=finaltxt.substr(0,finaltxt.length)+symb;
        }

        $('input.alias-field').val(finaltxt);
    },

    uploadBackImage: function() {
        $('.form_block').removeClass('active');
        $('.load_block').addClass('active');
    },

    sendFile: function(file, editor, welEditable) {
        data = new FormData();
        data.append("file", file);

        $.ajax({
            data: data,
            type: "POST",
            url: "/admin/"+document.URL.split('/')[4]+"/sendfile",
            cache: false,
            contentType: false,
            processData: false,
            success: function(url) {
                editor.insertImage(welEditable, url);
            }
        });
    },

    acceptPrice: function(target) {
        var input = target.parents('.price').find('input');
        var alias = input.data('serviceAlias');
        var price = input.val();
        var is_new = input.data('serviceIsNew');
        var is_premium = input.data('serviceIsPremium');

        $.post('/admin/service/'+alias+'/update', {Service: {alias: alias, price: price, is_new: is_new, is_premium: is_premium}});
        target.addClass('accepted');
    },

    appendServiceInfoBlock: function(target) {
        var last_info_block = target.prev('div:last');
        var cloned_block = last_info_block.clone(true, true);

        cloned_block.find('input').each(function() {
            var matches = [];
            $(this).attr('name').replace(/\[(.?)\]/g, function(g0,g1){matches.push(parseInt(g1)+1);});
            var new_name = $(this).attr('name').replace(/\[.?\]/g, '['+matches+']');
            $(this).attr('name', new_name);
        });

        cloned_block.find('input:last').remove();

        last_info_block.after(cloned_block);
    },

    removeServiceInfoBlock: function(id) {
        $.post("/service/removeInfoBlock", {service_info_block_id: id});
        window.location.reload();
    },

    appendRubric: function(target) {
        var cloned_rubric = target.prev('.rubric').clone();
        var cloned_rubric_id = parseInt(cloned_rubric.attr('id')) + 1;

        cloned_rubric.find('.rubric-remove').remove();
        cloned_rubric.find('.field-companycompanycategory-1-id').remove();
        cloned_rubric.attr('id', cloned_rubric_id);
        cloned_rubric.find('label').text('Рубрика #' + (cloned_rubric_id + 1));
        cloned_rubric.find('.field-companycompanycategory-1-category_id select')
            .attr('id', 'companycompanycategory-'+cloned_rubric_id+'-category_id')
            .attr('name', 'CompanyCompanyCategory['+cloned_rubric_id+'][category_id]');
        cloned_rubric.find('.field-companycompanycategory-1-subcategory_id select')
            .attr('id', 'companycompanycategory-'+cloned_rubric_id+'-subcategory_id')
            .attr('name', 'CompanyCompanyCategory['+cloned_rubric_id+'][subcategory_id]');
        target.before(cloned_rubric);
    },

    insertWorkingHours: function() {
        var from = $('#company-time-from').val();
        var to = $('#company-time-to').val();
        var working_hours_json = '{"from": "'+from+'", "to": "'+to+'"}';

        $('#company-working_hours').val(working_hours_json);
    },

    insertWorkingDays: function() {
        var days_json = [];
        var working_days = $('#company-weekend');
        var working_day = working_days.find('option');

        working_day.each(function(index) {
            if (working_day.eq(index).is(':checked')) {
                days_json.push(working_day.eq(index).val());
            }
        });

        $('#company-working_days').val(JSON.stringify(days_json));
    }
};